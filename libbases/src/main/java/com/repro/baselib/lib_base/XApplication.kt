package com.repro.baselib.lib_base

import com.repro.baselib.lib_base.network.RetrofitHelper
import com.repro.com.repro.lib_base.BuildConfig
import com.repro.lib_base.bases.BaseApplication

open class XApplication : BaseApplication() {
    override fun onCreate() {
        super.onCreate()
        //网络请求监控
        RetrofitHelper.init(this, isDebug = BuildConfig.DEBUG)
    }
}