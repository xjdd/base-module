package com.repro.baselib.lib_base.bean

/**
 */
 data class ConfigBean(
    val beautyConfig: BeautyConfig,
    val emojiConfig: EmojiConfig,
    val envConfig: EnvConfig,
    val imConfig: ImConfig,
    val rsConfig: RsConfig,
    val socketConfig: SocketConfig,
    val version: String,
    val giftConfig: GiftConfig
)

data class BeautyConfig(
    val version: Int
)

data class EmojiConfig(
    val version: String
)

data class EnvConfig(
    val apiUrl: String,
    val h5Url: String
)

data class ImConfig(
    val bigGroupId: String,
    val identifier: String,
    val privateKey: String,
    val sdkappid: String
)

data class RsConfig(
    val version: String
)

data class GiftConfig(
    val version:Int,
    val giftSvgaZipUrl:String
)

class SocketConfig(
)