package com.repro.baselib.lib_base.api

import com.repro.baselib.lib_base.bean.ConfigBean
import com.repro.baselib.lib_base.bean.LoginModel
import com.repro.baselib.lib_base.bean.RandomListBean
import com.repro.baselib.lib_base.constant.RetroNetConstant
import com.repro.lib_base.constant.RetroNetKey
import retrofit2.http.*

/**
 * 接口api
 */
interface texts {
    //可以做多域名切换
//    @Headers("${RetroNetKey.mRetroHostTypeKEY}:${RetroNetConstant.baseHost}")
//    @GET(value = "/config/test")
//    suspend fun getConfig(): ConfigBean


    //账号密码方式登录
    @FormUrlEncoded
    @POST("apis-user/authz/login/login")
    suspend fun loginByAccount(
        @Field(value = "account") account: String,
        @Field(value = "password") password: String
    ): LoginModel

    //随机卡界面资料卡列表
    @GET("apis-user/user/profile/random")
    suspend fun randomList(): MutableList<RandomListBean>
}