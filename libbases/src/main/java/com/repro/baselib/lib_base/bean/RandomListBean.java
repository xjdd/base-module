package com.repro.baselib.lib_base.bean;

import java.io.Serializable;

/**
 * 随机用户数据源
 */
public class RandomListBean implements Serializable {


    /**
     * userId : 851813273773800960
     * userCode : 51526174
     * nickname : Oprah
     * avatar : http://lk20test-new.oss-cn-hongkong.aliyuncs.com/131ce6912909b5cac8d7d99761cff4f1.jpg?x-oss-process=image/format,jpg
     * gender : 0
     * birthday : 631123200
     * level : 1
     * signature :
     * follows : 0
     * fansCount : 0
     */

    private long userId;
    private String userCode;
    private String nickname;
    private String avatar;
    private int gender;
    private String birthday;
    private int level;
    private int age;
    private String signature;
    private int follows;
    private int fansCount;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public int getFollows() {
        return follows;
    }

    public void setFollows(int follows) {
        this.follows = follows;
    }

    public int getFansCount() {
        return fansCount;
    }

    public void setFansCount(int fansCount) {
        this.fansCount = fansCount;
    }

    @Override
    public String toString() {
        return "RandomListBean{" +
                "userId=" + userId +
                ", userCode='" + userCode + '\'' +
                ", nickname='" + nickname + '\'' +
                ", avatar='" + avatar + '\'' +
                ", gender=" + gender +
                ", birthday='" + birthday + '\'' +
                ", level=" + level +
                ", age=" + age +
                ", signature='" + signature + '\'' +
                ", follows=" + follows +
                ", fansCount=" + fansCount +
                '}';
    }
}
