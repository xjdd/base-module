package com.repro.baselib.lib_base.utils

import android.content.Context
import android.content.pm.ApplicationInfo
import android.os.Build
import android.text.TextUtils
import com.repro.lib_base.utils.Md5Util
import java.util.*




/**
 *@date 2021/6/4 14:51
 *description: APP和手机相关信息获取
 */
object AppInfoUtil {

    const val CHANNEL_KEY = "cztchannel"

    const val appId = "3"

    /**
     * 渠道号
     */
    private var channel = ""

    /**
     * 版本Code
     */
    private var versionCode = ""

    /**
     * 版本Name
     */
    private var versionName = ""

    /**
     * 当前语言版本
     */
    private var languageCode = "en"

    /**
     * 设备唯一标识
     */
    private var imei = ""

    /**
     * 非3代表谷歌包
     */
    fun isGooglePackage(): Boolean {
        return appId != "3"
    }

    /**
     * 获取手机Android版本
     */
    fun getAndroidOS(): String {
        return "Android ${Build.VERSION.RELEASE}"
    }

    /**
     * 获取手机型号
     */
    fun getDeviceInfo(): String {
        return Build.BRAND + " " + Build.MODEL
    }

    /**
     * 获取应用的versionName
     */
    fun getVersionName(): String {
       return versionName
    }

    /**
     * 获取应用的versionCode
     */
    fun getVersionCode(): String {
        return versionCode
    }

    /**
     * 获取app的名称
     */
    fun getApplicationName(context: Context): String? {
        val applicationInfo: ApplicationInfo = context.applicationInfo
        val stringId = applicationInfo.labelRes
        return if (stringId == 0) applicationInfo.nonLocalizedLabel.toString() else context.getString(
            stringId
        )
    }


    /**
     * 获取当前系统语言
     * TODO  后续做多语言时做变更
     */
    fun getLanguageCode(): String {
        val language = Locale.getDefault().language
        languageCode = when (language) {
            "en" -> {
                "en"
            }
            "in" -> {
                "id"
            }
            else -> {
                "en"
            }
        }
        return languageCode
    }


    /**
     * 获取设备唯一标识
     */
    fun getDeviceIMEI(): String {
        if (!TextUtils.isEmpty(imei)) {
            return imei
        }
        return Md5Util.getMD5(System.currentTimeMillis().toString())
    }


    /**
     * 获取渠道号
     */
    fun getChannel(default: String = "ADD010"): String {
        return default
    }
}