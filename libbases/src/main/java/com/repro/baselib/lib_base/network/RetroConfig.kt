package com.repro.baselib.lib_base.network

import android.annotation.SuppressLint
import com.future.retronet.RetroNet.getGson
import com.future.retronet.config.BaseConfig
import com.future.retronet.interceptor.RetroLogInterceptor
import com.future.retronet.interceptor.RetryInterceptor
import com.readystatesoftware.chuck.ChuckInterceptor
import com.repro.baselib.lib_base.network.converterFactorys.RetroConverterFactory
import com.repro.baselib.lib_base.network.interceptors.BaseHostInterceptor
import com.repro.baselib.lib_base.network.interceptors.HahaRequestInterceptor
import com.repro.baselib.lib_base.network.interceptors.ParamsInterceptor
import com.repro.lib_base.utils.AppCache
import okhttp3.OkHttpClient
import org.apache.http.conn.ssl.AllowAllHostnameVerifier
import retrofit2.Retrofit

class RetroConfig : BaseConfig() {
    //可以在这里添加自定义拦截器
    @SuppressLint("AllowAllHostnameVerifier")
    override fun client(clientBuilder: OkHttpClient.Builder?) {
        clientBuilder?.let {
            if (RetrofitHelper.mDebug) {
                it.addNetworkInterceptor(RetroLogInterceptor())
                it.addInterceptor(ChuckInterceptor(AppCache.getContext()))
                it.hostnameVerifier(AllowAllHostnameVerifier())//所有证书都让其通过
            }
            it.addInterceptor(RetryInterceptor(5))
            it.addInterceptor(BaseHostInterceptor())
            it.addInterceptor(HahaRequestInterceptor())
            it.addInterceptor(ParamsInterceptor())
        }
    }

    override fun build(builder: Retrofit.Builder?) {
        builder?.addConverterFactory(RetroConverterFactory.create(getGson()))//自定义解析器,注意先后
        super.build(builder)
    }

    override fun getBaseUrl(): String {
        return RetrofitHelper.mBaseUrl//获取host
    }
}