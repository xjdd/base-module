package com.repro.baselib.lib_base.bean;

import java.util.List;

/**
 * id :100015
 * token : eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsInppcCI6IkRFRiJ9.eJx1jUEKwzAMBP-icwhW5EhBv0ljU9zQGGoLH0r-Xrfn9Lg7s-wbLAVQ8bJ44mUmJzghsx_AthwiKKBzDmcY4FFTj3tIx12txJfWvMfjYtvdYrfuXqLVwj-USumotTb-XsYtP7_tWkGRiYRp8nJ-AERTLhI.A28_leGilKwIjE1hIQoplClIjX7QgjcRzc96zyHupB8
 */
public class LoginModel {

    private String userId;
    private String userCode;
    private String nickname;
    private String avatar;
    private int gender;
    private String signature;
    private int userType;
    private String token;
    private String imUser;
    private String imSign;
    private String follows;
    private String fans;
    private int level;
    private String birthday;
    private int isUpStart;
    private int isBeautyUserCode;
    private String imBigGroupId;
    private String mobile;
    private List<WalletBean> wallet;

    public int getIsUpStart() {
        return isUpStart;
    }

    public void setIsUpStart(int isUpStart) {
        this.isUpStart = isUpStart;
    }

    public int getIsBeautyUserCode() {
        return isBeautyUserCode;
    }

    public void setIsBeautyUserCode(int isBeautyUserCode) {
        this.isBeautyUserCode = isBeautyUserCode;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getImUser() {
        return imUser;
    }

    public void setImUser(String imUser) {
        this.imUser = imUser;
    }

    public String getImSign() {
        return imSign;
    }

    public void setImSign(String imSign) {
        this.imSign = imSign;
    }

    public String getFollows() {
        return follows;
    }

    public void setFollows(String follows) {
        this.follows = follows;
    }

    public String getFans() {
        return fans;
    }

    public void setFans(String fans) {
        this.fans = fans;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getImBigGroupId() {
        return imBigGroupId;
    }

    public void setImBigGroupId(String imBigGroupId) {
        this.imBigGroupId = imBigGroupId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public List<WalletBean> getWallet() {
        return wallet;
    }

    public void setWallet(List<WalletBean> wallet) {
        this.wallet = wallet;
    }

    public static class WalletBean {
        /**
         * type : coin
         * amount : 11111
         * <p>
         * [
         * {
         * "type": "coin",
         * "amount": 11111
         * },
         * {
         * "type": "pearl",
         * "amount": 11111
         * }
         * ]
         */

        private String type;
        private String amount;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }
    }
}
