package com.repro.baselib.lib_base.constant

/**
 * 网络相关配置常量
 */
object RetroNetConstant {
    const val baseHost: String = "https://www.wanandroid.com"
    const val testId = ""
    const val testToken = ""
}