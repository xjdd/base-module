package com.repro.baselib.lib_base.network

import android.content.Context
import com.future.retronet.RetroNet
import com.repro.baselib.lib_base.constant.RetroNetConstant

/**
 * 网络请求协议helper帮助类
 * Retrofit
 */
object RetrofitHelper {
    lateinit var mBaseUrl: String
    var mDebug: Boolean = false

    fun init(context: Context, isDebug: Boolean, mBaseHost: String = "") {
        mDebug = isDebug
        mBaseUrl = if (mBaseHost.isNotEmpty()) {
            mBaseHost
        } else {
            RetroNetConstant.baseHost
        }
        RetroNet.init(context = context, RetroConfig::class.java, isDebug)
    }

}