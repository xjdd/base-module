### 日志更新时间2021.09月

##### config.gradle依赖库初步完成

config.gradle 用于管理项目配置与依赖

* gradle 基础插件的配置

* 1.更新基本android.sdk配置

* 2.增加Android官方的库

* 3.三方优秀的库和必备库

##### config.gradle使用方法

在project跟项目下找到`gradle.build`配置

```
apply from: "config.gradle" //这一行必须指定否者会找不到config.gradle 在module配置的时候会失败
// Top-level build file where you can add configuration options common to all sub-projects/modules.
buildscript {
    apply from: "config.gradle"//这一行（可选）用于配置plugin插件,用于统一管理
    repositories {
        google()
        mavenCentral()
    }
    dependencies {
        classpath pluginDependencies.toolsBuildGradle
        classpath pluginDependencies.kotlinGradlePlugin

        // NOTE: Do not place your application dependencies here; they belong
        // in the individual module build.gradle files
    }
}
```

模块中配置对应的依赖项目即可,方便统一管理以及查阅自己依赖的版本配置

```
plugins {
    id 'com.android.application'
    id 'kotlin-android'
}

android {
    compileSdkVersion projectConfig.compileSdkVersion
    buildToolsVersion projectConfig.buildToolsVersion

    defaultConfig {
        applicationId projectConfig.applicationId
        minSdkVersion projectConfig.minSdkVersion
        targetSdkVersion projectConfig.targetSdkVersion
        versionCode projectConfig.versionCode
        versionName projectConfig.versionName
    }

    buildTypes {
        release {
            minifyEnabled false
            proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
        }
    }
    compileOptions {
        sourceCompatibility JavaVersion.VERSION_1_8
        targetCompatibility JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = '1.8'
    }



    packagingOptions {
        //exclude 过滤掉某些文件或者目录不添加到APK中，作用于APK，不能过滤aar和jar中的内容。
        //pickFirst  匹配到多个相同文件，只提取第一个。只作用于APK，不能过滤aar和jar中的文件。
        //doNotStrip，可以设置某些动态库不被优化压缩。
        //merge，将匹配的文件都添加到APK中，和pickFirst有些相反，会合并所有文件。
        resources.excludes += "DebugProbesKt.bin"
    }

}

dependencies {

    implementation AndroidDependencies.Kotlin
    implementation AndroidDependencies.CoreKtx
    implementation AndroidDependencies.Appcompat
    implementation AndroidDependencies.Material
    implementation AndroidDependencies.ConstraintLayout
}
```

##### builderModule.gradle项目配置库

使用规则如下：`apply from: "../builderModule.gradle"`指向引入路径，没有此行无法进行公共绑定。

```
apply from: "../builderModule.gradle"//这一行是核心重点

android {
    //子类可以覆写
    resourcePrefix "${project.name}_"//资源前缀分开
}

dependencies {
    implementation AndroidDependencies.Kotlin
    implementation AndroidDependencies.CoreKtx
    implementation AndroidDependencies.Appcompat
    implementation AndroidDependencies.Material
    implementation AndroidDependencies.ConstraintLayout
}
```

builderModule.gradle说明 详情参考具体文件[builderModule.gradle](../builderModule.gradle)

```
/**
 * build.gradle 配置文件 用于管理当前gradle公共配置
 * 1.lib或者application切换
 * 2.SDK版本控制配置
 * 3.基础忽略文件配置
 * 4.java环境配置
 * 5.资源配置
 * 6.viewBinding or dataBinding
 * 7.签名配合
 * 以上配置完成之后在继承的子build.gradle可以复写。
 */
```

##### ToolbarView使用

toolbar创建需要覆写onToolbarSetting才能生效.

```kotlin 
            //Toolbar创建 
            ToolBarView.Builder()
            .setRequestTitle("你好，世界")//标题,默认""
            .setRequestTitleColor(R.color.color_191919)//标题颜色默认191919
            .setRequestTitleSize(20f)//字体大小默认20
            .setIsShowHomeAsUp(true)//是否展示返回按钮默认展示
            .setRequestNavigationIcon(R.mipmap.ic_launcher)//设置返回按钮的图片默认系统
            .setRequestToolbarMode(ToolbarMode.Parallel)//设置布局层级关系,层叠或者同级，默认ToolbarMode.Layer同级
            .setIsCenterTitleInToolbar(true)//是否居中显示默认居中
            .setRequestSubTitleInToolbar("subtitle")//副标题默认""
            .setRequestMenuId(R.menu.menu_tab)//展示右侧菜单
            .setOnMenuItemClickListener(this)//toolbar菜单监听
            .setRequestToolbarBg(R.color.white_50)//toolbar背景颜色默认透明
            .build()
```

##### MultiState页面状态的处理 [MultiStatePage](https://github.com/Zhao-Yan-Yan/MultiStatePage)

multiState 包含空页面(EmptyState)，错误页面(ErrorState)，加载中的页面(LoadingState)，以及加载成功页面(SuccessState)，通过监听网络不同状态或者页面需要展示不同的状态，加载对应的异常页面。



BaseStateActivity 实现绑定multiStateContainer界面

```
abstract class BaseStateActivity : FinalActivity() {
    lateinit var multiStateContainer: MultiStateContainer

    override fun initView(mContentView: View?) {

        multiStateContainer = MultiStatePage.bindMultiState(this) {
            if (ClickUtil.isClickAvailable()) {
                reLoadData()
            }
        }

    }

    /**
     * 点击错误页面会触发reLoadData数据需要重新加载数据.
     */
    abstract fun reLoadData()

}
```

Activity页面使用状态 选择对应的状态使用即可

```
//空页面
multiStateContainer.showEmpty<EmptyRepair>()
//加载中loading...
multiStateContainer.showLoading<LoadingRePlay>()
//成功状态取消mutliState
multiStateContainer.showSuccess<SuccessState>()
//错误页面
multiStateContainer.showError<ErrorState>()
```

覆写模板界面根据自身需求覆写模板实现自己想要的效果，没有覆写的时候默认展示模板

```
class LoadingRePlay : LoadingState() {
    override fun requestLayout(context: Context): Int {
        return R.layout.mult_state_loading
    }

    override fun onMultiStateViewCreate(view: View) {
        super.onMultiStateViewCreate(view)
        view.postDelayed({ clearMultiState() }, 6000)
    }
}
```



##### 注意事项:

1.libModule不继承`builder.gradle`配置 ,继承在导入后使用会出现编译错误。

错误：`AAPT2 aapt2-4.1.2-6503028-windows Daemon`