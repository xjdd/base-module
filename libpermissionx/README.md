#### PermissionCheck权限检查

PermissionCheck 权限检查基于PermissionX,可以自定义拦截通知权限提示dialog,动态申请权限,支持到android 12 os
permissions数组或者字符串
。allGranted 是否全部授权
。grantedList 已经授权的权限列表
。deniedList 没有授权的权限
```
private var permissions = listOf(Manifest.permission.RECORD_AUDIO)
permissionsCheck(permissions){ allGranted, grantedList, deniedList->
    DLog.e("=====$allGranted =$grantedList+ $deniedList")
}
```
