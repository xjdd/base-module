package com.repro.libpermissionx

import androidx.fragment.app.FragmentActivity
import com.permissionx.guolindev.PermissionX
import androidx.fragment.app.Fragment
import com.example.libpermissionx.R
import com.permissionx.guolindev.dialog.RationaleDialog
import com.permissionx.guolindev.dialog.RationaleDialogFragment
import com.permissionx.guolindev.request.PermissionBuilder


/**
 * 权限库的帮助类
 */
object PermissionHelper {

    /**
     * FragmentActivity 下检查权限 权限list
     */
    @JvmStatic
    fun permissionCheck(
        activity: FragmentActivity,
        isShowExplainRequestReason: Boolean,
        isShowForwardToSettings: Boolean,
        permissionList: List<String>,
        dialog: RationaleDialog?,
        dialogFragment: RationaleDialogFragment?,
        tipTitle: String?,
        positiveBtnString: String?,
        negativeBtnString: String?,
        callback: ((allGranted: Boolean?, grantedList: List<String>?, deniedList: List<String>?) -> Unit)?
    ) {
        val permissionBuilder = PermissionX.init(activity).permissions(permissionList)
        if (isShowExplainRequestReason) {
            //权限请求之后弹出告知用户请求权限作用
            permissionBuilder.onExplainRequestReason { scope, deniedList ->
                when {
                    dialog != null -> {
                        scope.showRequestReasonDialog(dialog)
                    }
                    dialogFragment != null -> {
                        scope.showRequestReasonDialog(dialogFragment)
                    }
                    else -> {
                        scope.showRequestReasonDialog(
                            deniedList,
                            tipTitle
                                ?: activity.resources.getString(R.string.permission_dialog_tipsTitle),
                            positiveBtnString
                                ?: activity.resources.getString(R.string.permission_dialog_positiveText),
                            negativeBtnString
                                ?: activity.resources.getString(R.string.permission_dialog_negativeText)
                        )
                    }
                }
            }
        }
        if (isShowForwardToSettings) {
            //勾选不在提示拦截请求权限说明
            permissionBuilder.onForwardToSettings { scope, deniedList ->
                when {
                    dialog != null -> {
                        scope.showForwardToSettingsDialog(dialog)
                    }
                    dialogFragment != null -> {
                        scope.showForwardToSettingsDialog(dialogFragment)
                    }
                    else -> {
                        scope.showForwardToSettingsDialog(
                            deniedList,
                            tipTitle
                                ?: activity.resources.getString(R.string.permission_dialog_tipsTitle),
                            positiveBtnString
                                ?: activity.resources.getString(R.string.permission_dialog_positiveText),
                            negativeBtnString
                                ?: activity.resources.getString(R.string.permission_dialog_negativeText)
                        )
                    }
                }
            }
        }
        permissionBuilder.request { allGranted, grantedList, deniedList ->
            if (callback != null) {
                callback(allGranted, grantedList, deniedList)
            }
        }
    }

    /**
     * Fragment 下检查权限 权限String
     */
    @JvmStatic
    fun permissionCheck(
        fragment: Fragment,
        isShowExplainRequestReason: Boolean,
        isShowForwardToSettings: Boolean,
        permissionList: List<String>,
        dialog: RationaleDialog?,
        dialogFragment: RationaleDialogFragment?,
        tipTitle: String?,
        positiveBtnString: String?,
        negativeBtnString: String?,
        callback: ((allGranted: Boolean, grantedList: List<String>, deniedList: List<String>) -> Unit)?
    ) {
        val permissionBuilder = PermissionX.init(fragment).permissions(permissionList)
        if (isShowExplainRequestReason) {
            permissionBuilder.onExplainRequestReason { scope, deniedList ->//拦截并提示
                when {
                    dialog != null -> {
                        scope.showRequestReasonDialog(dialog)
                    }
                    dialogFragment != null -> {
                        scope.showRequestReasonDialog(dialogFragment)
                    }
                    else -> {
                        scope.showRequestReasonDialog(
                            deniedList,
                            tipTitle
                                ?: fragment.resources.getString(R.string.permission_dialog_tipsTitle),
                            positiveBtnString
                                ?: fragment.resources.getString(R.string.permission_dialog_positiveText),
                            negativeBtnString
                                ?: fragment.resources.getString(R.string.permission_dialog_negativeText)
                        )
                    }
                }
            }
        }
        if (isShowForwardToSettings) {
            //勾选不在提示拦截请求权限说明并跳转设置
            permissionBuilder.onForwardToSettings { scope, deniedList ->
                when {
                    dialog != null -> {
                        scope.showForwardToSettingsDialog(dialog)
                    }
                    dialogFragment != null -> {
                        scope.showForwardToSettingsDialog(dialogFragment)
                    }
                    else -> {
                        scope.showForwardToSettingsDialog(
                            deniedList,
                            tipTitle
                                ?: fragment.resources.getString(R.string.permission_dialog_tipsTitle),
                            positiveBtnString
                                ?: fragment.resources.getString(R.string.permission_dialog_positiveText),
                            negativeBtnString
                                ?: fragment.resources.getString(R.string.permission_dialog_negativeText)
                        )
                    }
                }
            }
        }
        permissionBuilder.request { allGranted, grantedList, deniedList ->
            if (callback != null) {
                callback(allGranted, grantedList, deniedList)
            }
        }
    }

}