package com.repro.libpermissionx

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.permissionx.guolindev.dialog.RationaleDialog
import com.permissionx.guolindev.dialog.RationaleDialogFragment

/**
 * Fragment权限管理
 * @param permissionList 权限列表
 * @param isShowExplainRequestReason 申请权限之后给出系统提示
 * @param isShowForwardToSettings 权限勾选不在提示之后在申请时弹出弹窗提示权限
 * @param callback
 *          。allGranted 是否全部授权
 *          。grantedList 已经授权的权限列表
 *          。deniedList 没有授权的权限
 *
 */
fun Fragment.permissionsCheck(
    permissionList: List<String>,
    isShowExplainRequestReason: Boolean = false,
    isShowForwardToSettings: Boolean = true,
    dialog: RationaleDialog? = null,
    dialogFragment: RationaleDialogFragment? = null,
    tipTitle: String? = null,
    positiveBtnString: String? = null,
    negativeBtnString: String? = null,
    callback: ((allGranted: Boolean?, grantedList: List<String>?, deniedList: List<String>?) -> Unit)?
) {
    PermissionHelper.permissionCheck(
        this,
        isShowExplainRequestReason,
        isShowForwardToSettings,
        permissionList,
        dialog,
        dialogFragment,
        tipTitle,
        positiveBtnString,
        negativeBtnString,
        callback
    )
}

/**
 * Fragment权限管理
 * @param permissionList 权限列表
 * @param isShowExplainRequestReason 申请权限之后给出系统提示并会执行跳转到设置界面
 * @param isShowForwardToSettings 权限勾选不在提示之后在申请时弹出弹窗提示权限
 * @param callback
 *          。allGranted 是否全部授权
 *          。grantedList 已经授权的权限列表
 *          。deniedList 没有授权的权限
 *
 */
fun FragmentActivity.permissionsCheck(
    permissionList: List<String>,
    isShowExplainRequestReason: Boolean = false,
    isShowForwardToSettings: Boolean = true,
    dialog: RationaleDialog? = null,
    dialogFragment: RationaleDialogFragment? = null,
    tipTitle: String? = null,
    positiveBtnString: String? = null,
    negativeBtnString: String? = null,
    callback:((allGranted: Boolean?, grantedList: List<String>?, deniedList: List<String>?) -> Unit)?
) {
    PermissionHelper.permissionCheck(
        this,
        isShowExplainRequestReason,
        isShowForwardToSettings,
        permissionList,
        dialog,
        dialogFragment,
        tipTitle,
        positiveBtnString,
        negativeBtnString,
        callback
    )
}
