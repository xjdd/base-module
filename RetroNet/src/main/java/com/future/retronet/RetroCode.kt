package com.future.retronet


/**
 * @JvmField 消除了变量的getter与setter方法
 * @JvmField 修饰的变量不能是private属性的
 * @JvmStatic 只能在object类或者伴生对象companion object中使用，而@JvmField没有这些限制
 * @JvmStatic 一般用于修饰方法，使方法变成真正的静态方法；如果修饰变量不会消除变量的getter与setter方法，但会使getter与setter方法和变量都变成静态
 */
object RetroCode {
    //网络请求
    const val CODE_SUCCESS = 200                        //请求成功
    //自定状态码
    private const val CODE_CUSTOM = 60000
    const val CODE_PARSE_ERR = CODE_CUSTOM + 1          //解析错误
    const val CODE_REQUEST_CANCEL = CODE_CUSTOM + 2     //请求取消
    const val CODE_DOWNLOAD_ERR = CODE_CUSTOM + 3       //下载错误
    const val CODE_ERR_IO = CODE_CUSTOM + 5             //IO错误
    const val CODE_ERR_UNKNOWN = CODE_CUSTOM + 6        //未知错误
}