package com.future.retronet.uitls

import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type
import java.util.*

/**
 * 基于ParameterizedType实现泛型类类型参数化
 */
class ParameterizedTypeImpl(mActualTypeArguments: Array<Type>?, ownerType: Type?, rawType: Type?) : ParameterizedType{
    private var actualTypeArguments: Array<Type>? = mActualTypeArguments
    private var ownerType: Type? = ownerType
    private var rawType: Type? = rawType


    override fun getActualTypeArguments(): Array<Type>? {
        return actualTypeArguments
    }

    override fun getOwnerType(): Type? {
        return ownerType
    }

    override fun getRawType(): Type? {
        return rawType
    }


    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false
        val that: ParameterizedTypeImpl = o as ParameterizedTypeImpl

        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        if (!Arrays.equals(actualTypeArguments, that.actualTypeArguments)) return false
        if (if (ownerType != null) ownerType != that.ownerType else that.ownerType != null) return false
        return if (rawType != null) rawType == that.rawType else that.rawType == null
    }

    override fun hashCode(): Int {
        var result = if (actualTypeArguments != null) Arrays.hashCode(actualTypeArguments) else 0
        result = 31 * result + (ownerType?.hashCode() ?: 0)
        result = 31 * result + (rawType?.hashCode() ?: 0)
        return result
    }
}