package com.repro.lib.imageload

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.core.net.toUri
import coil.load
import coil.transform.CircleCropTransformation
import coil.transform.RoundedCornersTransformation
import com.bumptech.glide.Glide
import com.bumptech.glide.integration.webp.decoder.WebpDrawable
import com.bumptech.glide.integration.webp.decoder.WebpDrawableTransformation
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.MultiTransformation
import com.bumptech.glide.load.Transformation
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.repro.lib.imageload.impl.CoilEngine
import com.repro.lib.imageload.impl.GlideEngine
import com.repro.lib.imageload.transformers.CircleBorderTransform
import com.repro.lib.imageload.utils.Utils
import jp.wasabeef.glide.transformations.RoundedCornersTransformation as GlideRoundedCornersTransformation

/**
 * 图片加载
 * Coil模式下可以支持webp格式,默认模式是Glide是不会支持webp.需要
 * @See: ImageManager.initImageLoadOption(this)
 * @param url 图片链接
 */
fun ImageView.loadImage(url: String) {
    ImageManager.mImageEngine.loadImage(imageView = this, uri = url.toUri())
}

/**
 * 资源图片加载
 * @param resInt 图片资源
 */
fun ImageView.loadImage(@DrawableRes resInt: Int) {
    ImageManager.mImageEngine.loadImage(
        imageView = this,
        uri = Utils.drawableToUri(this.context, resInt)
    )
}


/**
 * 圆形效果
 * @param uri 连接
 * @param imageEngine 选择加载模式Glide or Coil
 */
fun ImageView.loadCircleCrop(uri: String, imageEngine: ImageEngine = GlideEngine.get()) {
    when (imageEngine.javaClass) {
        CoilEngine.get().javaClass -> {
            this.load(uri) {
                transformations(CircleCropTransformation())
            }
        }
        GlideEngine.get().javaClass -> {
            Glide.with(this.context)
                .load(uri)
                .placeholder(ImageManager.placeholder)
                .error(ImageManager.errorImage)
                .apply(RequestOptions.bitmapTransform(MultiTransformation(CircleCrop())))
                .into(this)
        }
    }
}

/**
 * 带圆环效果
 * @param url 加载地址
 * @param with 圆环宽度
 * @param color 圆环颜色
 */
fun ImageView.loadImageWithCircle(url: String, with: Int, color: Int) {
    Glide.with(context)
        .load(url)
        .placeholder(ImageManager.placeholder)
        .error(ImageManager.errorImage)
        .centerCrop()
        .transform(CircleBorderTransform(context, with, color))
        .into(this)
}

/**
 * 圆角
 * @param uri 连接
 * @param imageEngine 选择加载模式Glide or Coil
 * @param roundedRadius 圆角
 * 设置圆角不能设置imageView背景会导致圆角失败
 *  android:scaleType="centerCrop"在glide模式下会导致圆角失效
 */
fun ImageView.loadImageWithRoundedCorners(
    uri: String,
    roundedRadius: Int = 2,
    imageEngine: ImageEngine = GlideEngine.get()
) {
    when (imageEngine.javaClass) {
        GlideEngine.get().javaClass -> {
            Glide.with(context)
                .load(uri)
                .apply(
                    RequestOptions.bitmapTransform(
                        MultiTransformation(
                            GlideRoundedCornersTransformation(
                                roundedRadius, 0, GlideRoundedCornersTransformation.CornerType.ALL
                            )
                        )
                    )
                )
                .into(this);
        }
        CoilEngine.get().javaClass -> {
            this.load(uri) {
                transformations(
//                    GrayscaleTransformation(),//图片灰质
                    RoundedCornersTransformation(
                        topLeft = roundedRadius.toFloat(),
                        topRight = roundedRadius.toFloat(),
                        bottomLeft = roundedRadius.toFloat(),
                        bottomRight = roundedRadius.toFloat()
                    )
                )
            }
        }
    }
}


/**
 * glide webp加载模式

 * @param url 资源
 * @param wrapper 设置图片处理填充模式可以设置圆形 中间裁切或者设置圆角
 *                  val circleCrop: Transformation<Bitmap> = CircleCrop()//圆形裁切
 *                  val centerCrop: Transformation<Bitmap> = CenterCrop()//中间填充裁切
 *                  val centerCrop: Transformation<Bitmap> = RoundedCornersTransformation(20,0)//中间填充裁切
 * @param count 执行次数，默认-1无限循环
 */
fun ImageView.loadWebpImage(
    url: String,
    wrapper: Transformation<Bitmap> = CenterCrop(),
    count: Int = -1
) {
    Glide.with(context)
        .load(url)
        .placeholder(ImageManager.placeholder)
        .error(ImageManager.errorImage)
        .optionalTransform(WebpDrawable::class.java, WebpDrawableTransformation(wrapper))
        .addListener(object : RequestListener<Drawable?> {
            override fun onLoadFailed(
                e: GlideException?,
                model: Any?,
                target: Target<Drawable?>?,
                isFirstResource: Boolean
            ): Boolean {
                return false
            }

            override fun onResourceReady(
                resource: Drawable?,
                model: Any?,
                target: Target<Drawable?>?,
                dataSource: DataSource?,
                isFirstResource: Boolean
            ): Boolean {
                (resource as WebpDrawable).loopCount = count
                return false
            }
        })
        .into(this)
}