package com.repro.lib.imageload.impl

import android.content.Context
import android.graphics.drawable.Drawable
import android.net.Uri
import android.widget.ImageView
import coil.Coil
import coil.load
import coil.size.ViewSizeResolver
import com.repro.lib.imageload.ImageEngine

class CoilEngine : ImageEngine {
    companion object {
        private var instance: CoilEngine? = null
            get() {
                if (field == null) {
                    field = CoilEngine()
                }
                return field
            }

        fun get(): CoilEngine {
            return instance!!
        }
    }

    override fun loadThumbnail(
        resize: Int,
        placeholder: Drawable?,
        imageView: ImageView?,
        uri: Uri?
    ) {
        imageView?.load(uri) {
            size(ViewSizeResolver(imageView))
            placeholder(placeholder)
        }
    }

    override fun loadGifThumbnail(
        resize: Int,
        placeholder: Drawable?,//占位图
        imageView: ImageView?,
        uri: Uri?
    ) {
        imageView?.load(uri) {
            size(ViewSizeResolver(imageView))
            placeholder(placeholder)
        }
    }

    override fun loadImage(
        resizeX: Int,
        resizeY: Int,
        imageView: ImageView?,
        uri: Uri?
    ) {
        imageView?.load(uri) {
            size(ViewSizeResolver(imageView))
        }

    }

    override fun loadGifImage(
        resizeX: Int,
        resizeY: Int,
        imageView: ImageView?,
        uri: Uri?
    ) {
        imageView?.load(uri) {
            size(ViewSizeResolver(imageView))
        }
    }


    override fun supportAnimatedGif(): Boolean {
        return true
    }

    override fun <T> getImageObject(context: Context?): T {
        return Coil as T
    }

    override fun clearCache(context: Context?) {
    }
}