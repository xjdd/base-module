package com.repro.lib.imageload.impl

import android.content.Context
import android.graphics.drawable.Drawable
import android.net.Uri
import android.widget.ImageView
import com.repro.lib.imageload.ImageEngine
import com.bumptech.glide.request.RequestOptions

import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.repro.lib.imageload.ImageManager
import java.lang.RuntimeException

/**
 * ImageEngine for implementation user glide
 */
class GlideEngine() : ImageEngine {
    companion object {
        private var instance: GlideEngine? = null
            get() {
                if (field == null) {
                    field = GlideEngine()
                }
                return field
            }

        fun get(): GlideEngine {
            return instance!!
        }
    }

    override fun loadThumbnail(
        resize: Int,
        placeholder: Drawable?,
        imageView: ImageView?,
        uri: Uri?
    ) {
        imageView?.let {
            Glide.with(it.context)
                .asBitmap() // some .jpeg files are actually gif
                .load(uri)
                .error(ImageManager.placeholder)
                .placeholder(ImageManager.placeholder)
                .apply(
                    RequestOptions()
                        .override(resize, resize)
                        .placeholder(placeholder)
                        .centerCrop()
                )
                .into(it)
        }
    }

    override fun loadGifThumbnail(
        resize: Int,
        placeholder: Drawable?,
        imageView: ImageView?,
        uri: Uri?
    ) {
        imageView?.let {
            Glide.with(it.context)
                .asBitmap() // some .jpeg files are actually gif
                .load(uri)
                .error(ImageManager.placeholder)
                .placeholder(ImageManager.placeholder)
                .apply(
                    RequestOptions()
                        .override(resize, resize)
                        .placeholder(placeholder)
                        .centerCrop()
                )
                .into(it)
        }
    }

    override fun loadImage(
        resizeX: Int,
        resizeY: Int,
        imageView: ImageView?,
        uri: Uri?
    ) {
        imageView?.let {
            Glide.with(it.context)
                .load(uri)
                .error(ImageManager.placeholder)
                .placeholder(ImageManager.placeholder)
                .apply(
                    RequestOptions()
                        .override(resizeX, resizeY)
                        .priority(Priority.HIGH)
                        .centerCrop()
                )
                .into(it)
        }
    }

    override fun loadGifImage(
        resizeX: Int,
        resizeY: Int,
        imageView: ImageView?,
        uri: Uri?
    ) {
        imageView?.let {
            Glide.with(it.context)
                .asGif()
                .load(uri)
                .error(ImageManager.placeholder)
                .placeholder(ImageManager.placeholder)
                .apply(
                    RequestOptions()
                        .override(resizeX, resizeY)
                        .priority(Priority.HIGH)
                        .centerCrop()
                )
                .into(it)
        }
    }

    override fun supportAnimatedGif(): Boolean {
        return true
    }

    override fun <T> getImageObject(context: Context?): T {
        if (context == null) {
            throw RuntimeException("缺少context!!!!")
        }
        return Glide.get(context) as T
    }

    /**
     * 清空内存缓存
     * @param context Context
     */
    fun clearMemory(context: Context?) {
        context?.let {
            Glide.get(it).clearMemory()
        }
    }


    /**
     * 清空内存缓存
     * @param context Context
     */
    fun clearDiskCache(context: Context?) {
        context?.let {
            Glide.get(it).clearDiskCache()
        }
    }

    override fun clearCache(context: Context?) {
        clearMemory(context)
        clearDiskCache(context)
    }
}