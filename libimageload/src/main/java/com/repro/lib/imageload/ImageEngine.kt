package com.repro.lib.imageload

import android.content.Context
import android.graphics.drawable.Drawable
import android.net.Uri
import android.widget.ImageView


/**
 * ImageLoad接口
 */
interface ImageEngine {

    /**
     * 加载静态图片资源的缩略图
     *
     * @param resize      调整原始图像的所需大小
     * @param placeholder 占位符在图像尚未加载时可绘制
     * @param imageView   ImageView构件
     * @param uri         uri加载镜像的URI
     */
    fun loadThumbnail(
        resize: Int = -1,
        placeholder: Drawable?,
        imageView: ImageView?,
        uri: Uri?
    )

    /**
     *  加载gif图片资源的缩略图。加载gif缩略图
     *
     * @param resize      调整原始图像的所需大小
     * @param placeholder 占位符在图像尚未加载时可绘制
     * @param imageView   ImageView控件。
     * @param uri         uri加载镜像的URI
     */
    fun loadGifThumbnail(
        resize: Int = -1,
        placeholder: Drawable?,
        imageView: ImageView?,
        uri: Uri?
    )

    /**
     * 加载静态图片资源。
     *
     * @param resizeX   X原始图像的所需x大小。
     * @param resizeY   Y所需的原始图像的y大小。
     * @param imageView ImageView控件。
     * @param uri       uri加载镜像的URI。
     */
    fun loadImage(
        resizeX: Int = -1,
        resizeY: Int = -1,
        imageView: ImageView?,
        uri: Uri?
    )


    /**
     * 加载gif图片资源。
     *
     * @param context   Context
     * @param resizeX   X原始图像的所需x大小。
     * @param resizeY   Y所需的原始图像的y大小。
     * @param imageView ImageView控件。
     * @param uri       Uri of the loaded image
     */
    fun loadGifImage(
        resizeX: Int = -1,
        resizeY: Int = -1,
        imageView: ImageView?,
        uri: Uri?
    )

    /**
     * 该实现是否支持动画gif.
     * 只需了解即可，方便用户使用。
     *
     * @return true支持动画gif，false不支持动画gif。
     */
    fun supportAnimatedGif(): Boolean

    /**
     * 这里获取Coil对象
     */
    fun <T> getImageObject(context: Context?): T


    fun clearCache(context: Context?)
}