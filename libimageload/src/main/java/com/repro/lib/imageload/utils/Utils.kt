package com.repro.lib.imageload.utils

import android.content.ContentResolver
import android.content.Context
import android.content.res.Resources
import android.net.Uri
import androidx.annotation.DrawableRes


object Utils {
    @JvmStatic
    fun drawableToUri(mContext: Context, @DrawableRes rawRes: Int): Uri {
        val r: Resources = mContext.applicationContext.resources
        return Uri.parse(
            ContentResolver.SCHEME_ANDROID_RESOURCE + "://"
                    + r.getResourcePackageName(rawRes) + "/"
                    + r.getResourceTypeName(rawRes) + "/"
                    + r.getResourceEntryName(rawRes)
        )
    }

    @JvmStatic
    fun stringToUri(str: String): Uri {
        return Uri.parse(str)
    }
}