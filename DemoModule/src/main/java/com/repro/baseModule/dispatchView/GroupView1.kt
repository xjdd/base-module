package com.repro.baseModule.dispatchView

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.widget.LinearLayout
import com.repo.utils.DLog

/**
 * 自定义view一定要实现两个参数的方法否则会报错
 *      Caused by: android.view.InflateException:
 *          Binary XML file line #22 in com.repro.baseModule:layout/demo_activity_view_dispatch:
 *          Binary XML file line #22 in com.repro.baseModule:layout/demo_activity_view_dispatch:
 *          Error inflating class com.repro.baseModule.dispatchView.GroupView1
 *      Caused by: android.view.InflateException:
 *          Binary XML file line #22 in com.repro.baseModule:layout/demo_activity_view_dispatch:
 *          Error inflating class com.repro.baseModule.dispatchView.GroupView1
 */
class GroupView1(context: Context?,attributes: AttributeSet?) :
    LinearLayout(context,attributes) {

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        DLog.e("GroupView1----->dispatchTouchEvent ")
        return super.dispatchTouchEvent(ev)
    }

    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        DLog.e("GroupView1----->onInterceptTouchEvent")
        return super.onInterceptTouchEvent(ev)
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        DLog.e("GroupView1----->onTouchEvent")
        return super.onTouchEvent(event)
    }
}