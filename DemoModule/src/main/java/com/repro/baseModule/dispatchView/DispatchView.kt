package com.repro.baseModule.dispatchView

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import com.repo.utils.DLog

class DispatchView(context: Context?,attributes: AttributeSet?) : View(context,attributes) {
    override fun dispatchTouchEvent(event: MotionEvent?): Boolean {
        DLog.e("DispatchView----->dispatchTouchEvent ")
        return super.dispatchTouchEvent(event)

    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        DLog.e("DispatchView----->onTouchEvent ")
        return false
    }
}