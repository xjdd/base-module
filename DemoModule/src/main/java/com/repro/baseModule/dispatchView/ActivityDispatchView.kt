package com.repro.baseModule.dispatchView

import android.os.Bundle
import com.repo.utils.DLog
import com.repro.baseModule.databinding.DemoActivityViewDispatchBinding
import com.repro.lib_base.bases.activity.BaseActivity
import com.repro.lib_base.bases.activity.ToolBarView
import com.repro.lib_base.liveData.LiveDataBusEvent
import com.repro.libtoast.ToastUtil
import kotlinx.coroutines.*


/**
 *
 * 如果出现Caused by: java.lang.IllegalArgumentException: Cannot add the same observer with different lifecycles
 * 检查是否观察写的是空的观察者
 *       LiveDataBusEvent.get().with("nihao2", Int::class.java)
 *               .observe(this, { t ->
 *                 //TDOD 这里一定要写方法如果只是打印会导致出现以上错误
 *                  DLog.e("----$t+====>${t.javaClass}")
 *      })
 */
class ActivityDispatchView : BaseActivity<DemoActivityViewDispatchBinding>() {
    override fun onToolbarSetting(): ToolBarView? {
        return null
    }

    override fun observeLiveData() {

    }


    override fun setViewData(savedInstanceState: Bundle?) {
        LiveDataBusEvent.get().getChannel("nihao2", String::class.java)
            .observe(this@ActivityDispatchView, { t ->
                DLog.e("----$t+====>ActivityDispatchView")
                ToastUtil.show(this@ActivityDispatchView, t)
            })
        LiveDataBusEvent.get().getChannel("nihao2",String::class.java).setValue("value")
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
    }
}