package com.repro.baseModule

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.Toolbar
import com.repo.utils.DLog
import com.repro.baseModule.databinding.ActivityMainBinding
import com.repro.baseModule.dispatchView.ActivityDispatchView
import com.repro.baseModule.stateViews.LoadingRePlay
import com.repro.baseModule.vm.testViewModel
import com.repro.lib.imageload.*
import com.repro.lib_base.bases.activity.BaseDataBindActivity
import com.repro.lib_base.bases.activity.ToolBarView
import com.repro.lib_base.bases.activity.ToolbarMode
import com.repro.lib_base.bases.vm.createViewModel
import com.repro.libpermissionx.permissionsCheck
import com.repro.libstate.state.SuccessState
import com.repro.libtoast.ToastUtil
import kotlinx.coroutines.*

class MainActivity : BaseDataBindActivity<ActivityMainBinding>(), Toolbar.OnMenuItemClickListener {

    override fun onToolbarSetting(): ToolBarView? {
        setThemes(darkFont = true, fitSystemWindow = true)
        return ToolBarView.Builder()
            .setRequestTitle("你好，世界")//标题
            .setRequestTitleColor(R.color.color_191919)//标题颜色
            .setRequestTitleSize(20f)//字体大小
            .setIsShowHomeAsUp(true)//是否展示返回按钮默认展示
            .setRequestToolbarMode(ToolbarMode.Parallel)//设置布局层级关系,层叠或者同级
            .setIsCenterTitleInToolbar(true)//是否居中显示
            .setRequestSubTitleInToolbar("subtitle")//副标题
            .setRequestMenuId(R.menu.menu_tab)//展示右侧菜单
            .setOnMenuItemClickListener(this)//toolbar菜单监听
            .setRequestToolbarBg(R.color.white_50)//toolbar背景颜色
            .build()
    }

    private val mTestViewModel by lazy {
        createViewModel<testViewModel>()
    }

    override fun observeLiveData() {
        mTestViewModel.getShowToastLiveData().observe(this, {
            it?.let {
                ToastUtil.showDebug(getActivityContext(), it)
            }
        })
        mTestViewModel.getShowLoadingLiveData().observe(this, {
            if (it) {
                multiStateContainer.show<LoadingRePlay>()
            } else {
                multiStateContainer.show<SuccessState>()
            }
        })
    }

    override fun setViewData(savedInstanceState: Bundle?) {

    }

    override fun initView(mContentView: View?) {
        super.initView(mContentView)
        binding.text.text = "你好，D世界！"
        binding.image.loadImage("https://p.upyun.com/demo/webp/webp/animated-gif-0.webp")

    }

    override fun onResume() {
        super.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    private var permissions =
        listOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO)

    override fun onMenuItemClick(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.follow -> {
                permissionsCheck(permissions) { allGranted, grantedList, deniedList ->
                    DLog.e("=====$allGranted =$grantedList+ $deniedList")
                }
            }
            R.id.addBlack -> {
                startActivity(Intent(this@MainActivity, ActivityDispatchView::class.java))
            }
            R.id.report -> {
                mTestViewModel.getConfig()
            }
        }
        return true
    }

    override fun requestLayoutId(): Int {
        return R.layout.activity_main
    }
}