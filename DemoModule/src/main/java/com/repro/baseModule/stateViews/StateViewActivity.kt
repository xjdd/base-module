package com.repro.baseModule.stateViews

import android.os.Bundle
import com.repro.baseModule.databinding.ActivityStateViewBinding
import com.repro.lib_base.bases.activity.BaseActivity
import com.repro.lib_base.bases.activity.ToolBarView
import com.repro.libstate.showEmpty
import com.repro.libstate.showError
import com.repro.libstate.showLoading
import com.repro.libstate.showSuccess
import com.repro.libstate.state.ErrorState
import com.repro.libstate.state.SuccessState

class StateViewActivity : BaseActivity<ActivityStateViewBinding>() {

    override fun reLoadData() {
        multiStateContainer.showSuccess<SuccessState>()
    }

    override fun onToolbarSetting(): ToolBarView? {
        return null
    }

    override fun observeLiveData() {

    }

    override fun setViewData(savedInstanceState: Bundle?) {
        binding.btnEmpty.setOnClickListener {
            multiStateContainer.showEmpty<EmptyRepair>()

        }
        binding.btnLoading.setOnClickListener {
            multiStateContainer.showLoading<LoadingRePlay>()

        }
        binding.btnSuccess.setOnClickListener {
            multiStateContainer.showSuccess<SuccessState>()

        }
        binding.btnError.setOnClickListener {
            multiStateContainer.showError<ErrorState>()

        }
    }
}