package com.repro.baseModule.stateViews

import android.content.Context
import android.view.View
import com.repro.baseModule.R
import com.repro.libstate.state.LoadingState

class LoadingRePlay : LoadingState() {
    override fun requestLayout(context: Context): Int {
        return R.layout.mult_state_loading
    }

    override fun onMultiStateViewCreate(view: View) {
        super.onMultiStateViewCreate(view)
        view.postDelayed({ clearMultiState() }, 6000)
    }
}