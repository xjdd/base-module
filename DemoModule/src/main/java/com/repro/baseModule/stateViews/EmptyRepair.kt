package com.repro.baseModule.stateViews

import android.content.Context
import com.repro.baseModule.R
import com.repro.libstate.state.EmptyState

class EmptyRepair : EmptyState() {
    override fun requestLayout(context: Context): Int {
        return R.layout.activity_test
    }
}