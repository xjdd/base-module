package com.repro.baseModule.vm

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.future.retronet.RetroNet
import com.repro.baselib.lib_base.api.texts
import com.repro.baselib.lib_base.bean.RandomListBean
import com.repro.lib_base.bases.vm.BaseViewModel

class testViewModel(application: Application) : BaseViewModel(application) {

    var testData : MutableLiveData<List<RandomListBean>>  =  MutableLiveData<List<RandomListBean>>()

     fun getConfig() {
        retroNet(true) {
            doWork {
                val data = RetroNet.create(texts::class.java).randomList()
                getShowToastLiveData().value = data[0].avatar
                testData.value = data
            }
        }
    }
}