package com.repro.baseModule

import com.repo.utils.DLog
import com.repro.baselib.lib_base.XApplication
import com.repro.libstate.MultiStateConfig
import com.repro.libstate.MultiStatePage

class DemoApplication : XApplication() {
    override fun onCreate() {
        super.onCreate()
        DLog.e("------模块的Demo------")
        initMultiState()
    }

    /**
     * 初始化缺省页的方法
     */
    private fun initMultiState() {
        val config = MultiStateConfig.Builder()
            .alphaDuration(300)
            .errorIcon(R.mipmap.state_empty_error)
            .loadingMsg("loading")
            .retryMsg("点击重试")
            .build()
        MultiStatePage.config(config)
    }
}