package com.repro.baseModule.fragment

import android.os.Bundle
import android.view.View
import com.repro.baseModule.databinding.DemoFragmentTestBinding
import com.repro.baseModule.stateViews.LoadingRePlay
import com.repro.lib_base.bases.fragment.BaseFragment

class TestFragment : BaseFragment<DemoFragmentTestBinding>() {

    override fun initData(arguments: Bundle?) {
    }

    override fun initView(rootView: View?) {
        binding?.tvText2?.setOnClickListener {
            multiStateContainer.show<LoadingRePlay>()
        }
    }

    override fun setViewData(savedInstanceState: Bundle?) {
    }

    override fun reLoadData() {
    }
}