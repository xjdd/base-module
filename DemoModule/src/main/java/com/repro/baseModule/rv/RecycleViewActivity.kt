package com.repro.baseModule.rv

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.repro.baseModule.R
import com.repro.baseModule.databinding.DemoActivityRvBinding
import com.repro.baseModule.rv.adapter.FollowsListAdapter
import com.repro.baseModule.vm.testViewModel
import com.repro.baselib.lib_base.bean.RandomListBean
import com.repro.lib_base.bases.activity.BaseRVActivity
import com.repro.lib_base.bases.activity.ToolBarView
import com.repro.lib_base.bases.activity.ToolbarMode
import com.repro.lib_base.bases.views.SmartRecyclerView
import com.repro.lib_base.bases.vm.createViewModel
import com.repro.libstate.state.SuccessState

class RecycleViewActivity : BaseRVActivity<DemoActivityRvBinding, RandomListBean>() {
    override val smartRecyclerView: SmartRecyclerView<RandomListBean> by lazy {
        binding.demoRecyclerView2 as SmartRecyclerView<RandomListBean>
    }

    override val adapter: BaseQuickAdapter<RandomListBean, *> by lazy {
        FollowsListAdapter()
    }

    override val layoutManager: RecyclerView.LayoutManager by lazy {
        LinearLayoutManager(this)
    }

    override val loadData: (page: Int) -> Unit = {
        mate.getConfig()
    }

    private val mate by lazy {
        createViewModel<testViewModel>()
    }


    override fun onToolbarSetting(): ToolBarView? {
        setThemes(darkFont = true, fitSystemWindow = true)
        return ToolBarView.Builder()
            .setRequestTitle("你好，世界")//标题
            .setRequestTitleColor(R.color.color_191919)//标题颜色
            .setRequestTitleSize(20f)//字体大小
            .setIsShowHomeAsUp(true)//是否展示返回按钮默认展示
            .setRequestNavigationIcon(R.mipmap.ic_launcher)//设置返回按钮的图片
            .setRequestToolbarMode(ToolbarMode.Layer)//设置布局层级关系,层叠或者同级
            .setIsCenterTitleInToolbar(true)//是否居中显示
            .setRequestSubTitleInToolbar("subtitle")//副标题
            .setRequestToolbarBg(R.color.white_50)//toolbar背景颜色
            .build()
    }

    override fun observeLiveData() {
        mate.getShowLoadingLiveData().observe(this) {
            if (!it) {
                multiStateContainer.show<SuccessState>()
            }
        }
        mate.testData.observe(this){
            smartRecyclerView.onFetchDataFinish(it,true)
        }


    }

    override fun setViewData(savedInstanceState: Bundle?) {


    }


}