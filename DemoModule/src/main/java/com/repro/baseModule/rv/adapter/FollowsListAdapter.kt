package com.repro.baseModule.rv.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.module.LoadMoreModule
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.repro.baselib.lib_base.bean.RandomListBean

/**
 * 新关注列表适配器
 * 3.0默认需要导入loadMoreModule
 */
class FollowsListAdapter :
    BaseQuickAdapter<RandomListBean, BaseViewHolder>(android.R.layout.activity_list_item) ,LoadMoreModule{
    override fun convert(helper: BaseViewHolder, item: RandomListBean) {
        helper.setText(android.R.id.text1, item.nickname)
    }

}