package com.repro.baseModule

import android.os.Bundle
import android.view.View
import com.repro.baseModule.databinding.ActivityTestBinding
import com.repro.baseModule.fragment.TestFragment
import com.repro.lib_base.bases.activity.BaseActivity
import com.repro.lib_base.bases.activity.ToolBarView
import com.repro.lib_base.bases.activity.ToolbarMode

class FragmentActivity:BaseActivity<ActivityTestBinding>() {
    override fun onToolbarSetting(): ToolBarView? {
        setThemes(darkFont = true, fitSystemWindow = false)
        return ToolBarView.Builder()
            .setRequestTitle("你好，世界")//标题
            .setRequestTitleColor(R.color.color_191919)//标题颜色
            .setRequestTitleSize(20f)//字体大小
            .setIsShowHomeAsUp(true)//是否展示返回按钮默认展示
            .setRequestNavigationIcon(R.mipmap.ic_launcher)//设置返回按钮的图片
            .setRequestToolbarMode(ToolbarMode.Parallel)//设置布局层级关系,层叠或者同级
            .setIsCenterTitleInToolbar(true)//是否居中显示
            .setRequestSubTitleInToolbar("subtitle")//副标题
            .setRequestToolbarBg(R.color.white_50)//toolbar背景颜色
            .build()
    }

    override fun observeLiveData() {
    }

    override fun initView(mContentView: View?) {
        super.initView(mContentView)
        val testFragment = TestFragment()
        supportFragmentManager.beginTransaction().add(R.id.frame_constraintss,testFragment).commit()
    }

    override fun setViewData(savedInstanceState: Bundle?) {

    }
}