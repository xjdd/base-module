package com.repro.libstate.state

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.DrawableRes
import com.repro.libstate.MultiState
import com.repro.libstate.MultiStateContainer
import com.repro.libstate.MultiStatePage
import com.repro.libstate.R

/**
 * 错误展示页面模板
 */
open class ErrorState : MultiState() {
    private var imgError: ImageView? = null
    private var tvRetry: TextView? = null

    override fun requestLayout(context: Context): Int {
        return R.layout.mult_state_error
    }

    override fun onMultiStateViewCreate(view: View) {
        imgError = view.findViewById(R.id.img_error)
        tvRetry = view.findViewById(R.id.tv_retry)

        setRetryMsg(MultiStatePage.config.errorMsg)
        setErrorIcon(MultiStatePage.config.errorIcon)
    }

    override fun enableReload(): Boolean {
        return true
    }

    override fun bindRetryView(): View? {
        return null
    }

    private fun setRetryMsg(errorMsg: String) {
        tvRetry?.text = errorMsg
    }

    private fun setErrorIcon(@DrawableRes errorIcon: Int) {
        imgError?.setImageResource(errorIcon)
    }
}