package com.repro.libstate.state

import android.content.Context
import android.view.View
import android.widget.ImageView
import androidx.annotation.DrawableRes
import com.repro.libstate.MultiState
import com.repro.libstate.MultiStatePage
import com.repro.libstate.R


/**
 * 空页面模板
 */
open class EmptyState : MultiState() {
    private var imgEmpty: ImageView? = null

    override fun requestLayout(context: Context): Int {
        return R.layout.mult_state_empty
    }

    override fun enableReload(): Boolean {
        return true
    }

    override fun bindRetryView(): View? {
        return imgEmpty
    }

    override fun onMultiStateViewCreate(view: View) {
        imgEmpty = view.findViewById(R.id.img_empty)
        setEmptyIcon(MultiStatePage.config.emptyIcon)
    }

    private fun setEmptyIcon(@DrawableRes emptyIcon: Int) {
        imgEmpty?.setImageResource(emptyIcon)
    }
}