package com.repro.libstate.state

import android.content.Context
import android.view.View
import com.repro.libstate.MultiState

/**
 * 成功展示界面
 */
open class SuccessState : MultiState() {
    override fun requestLayout(context: Context): Int {
        return 0
    }


    override fun onMultiStateViewCreate(view: View) = Unit

}