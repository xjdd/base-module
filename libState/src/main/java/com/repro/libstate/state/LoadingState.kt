package com.repro.libstate.state

import android.content.Context
import android.view.View
import android.widget.TextView
import com.repro.libstate.MultiState
import com.repro.libstate.MultiStatePage
import com.repro.libstate.R

/**
 * 加载中页面模板
 * 设置一个默认时间如果超过这个时间则隐藏加载界面
 */
open class LoadingState : MultiState() {
    private var tvLoadingMsg: TextView? = null
    override fun requestLayout(context: Context): Int {
        return R.layout.mult_state_loading
    }

    override fun onMultiStateViewCreate(view: View) {
        tvLoadingMsg = view.findViewById(R.id.tv_loading_msg)
        setLoadingMsg(MultiStatePage.config.loadingMsg)
    }

    open fun setLoadingMsg(loadingMsg: String) {
        tvLoadingMsg?.text = loadingMsg
    }
}