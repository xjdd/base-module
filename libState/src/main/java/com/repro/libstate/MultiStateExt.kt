package com.repro.libstate

import android.app.Activity
import android.view.View

fun View.bindMultiState(onRetryEventListener: OnRetryEventListener = OnRetryEventListener { }) =
    MultiStatePage.bindMultiState(this, onRetryEventListener)

fun Activity.bindMultiState(onRetryEventListener: OnRetryEventListener = OnRetryEventListener { }) =
    MultiStatePage.bindMultiState(this, onRetryEventListener)


/**
 * MultiStateContainer
 */
inline fun <reified T : MultiState> MultiStateContainer.showSuccess(
    enableAnimator: Boolean = true,
    crossinline callBack: () -> Unit = {}
) {
    show<T>(enableAnimator) {
        callBack.invoke()
    }
}

inline fun <reified T : MultiState> MultiStateContainer.showError(
    enableAnimator: Boolean = true,
    crossinline callBack: () -> Unit = {}
) {
    show<T>(enableAnimator) {
        callBack.invoke()
    }
}

inline fun <reified T : MultiState> MultiStateContainer.showEmpty(
    crossinline callBack: () -> Unit = {}
) {
    show<T> {
        callBack.invoke()
    }
}

inline fun <reified T : MultiState> MultiStateContainer.showLoading(
    enableAnimator: Boolean = true,
    crossinline callBack: () -> Unit = {}
) {
    show<T>(enableAnimator) {
        callBack.invoke()
    }
}