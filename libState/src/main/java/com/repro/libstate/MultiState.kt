package com.repro.libstate

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import com.repro.libstate.state.SuccessState

/**
 * multiState抽象类
 */
abstract class MultiState {

    abstract fun requestLayout(context: Context): Int
    private var mContainer: MultiStateContainer? = null

    /**
     * 创建stateView
     */
    fun onCreateMultiStateView(
        context: Context,
        inflater: LayoutInflater,
        container: MultiStateContainer
    ): View {
        this.mContainer = container
        return if (requestLayout(context) == 0) {
            View(context)
        } else {
            inflater.inflate(requestLayout(context), container, false)
        }
    }


    /**
     * stateView创建完成
     */
    abstract fun onMultiStateViewCreate(view: View)

    /**
     * 是否允许重新加载 点击事件
     * 默认false 不允许
     */
    open fun enableReload(): Boolean = false

    /**
     * 绑定重试view
     * 默认null为整个state view
     */
    open fun bindRetryView(): View? = null

    /**
     * 清理掉MultiState
     */
    open fun clearMultiState() {
        mContainer?.show<SuccessState>()
    }
}