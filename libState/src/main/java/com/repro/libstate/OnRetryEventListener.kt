package com.repro.libstate

fun interface OnRetryEventListener {
    fun onRetryEvent(multiStateContainer: MultiStateContainer)
}