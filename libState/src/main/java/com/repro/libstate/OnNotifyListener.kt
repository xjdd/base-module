package com.repro.libstate

fun interface OnNotifyListener<T : MultiState> {
    fun onNotify(multiState: T)
}