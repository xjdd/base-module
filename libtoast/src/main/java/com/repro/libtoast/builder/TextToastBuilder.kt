package com.repro.libtoast.builder

import android.content.Context
import android.widget.Toast

class TextToastBuilder(context: Context) : ToastBuilder(context) {
    private var msgStr: CharSequence? = null

    fun setMsgStr(msgStr: CharSequence?): TextToastBuilder {
        this.msgStr = msgStr
        return this
    }

    override fun build(): Toast? {
        val toast = Toast.makeText(context, msgStr, duration)
        toast.setGravity(gravity, xOffset, yOffset)
        toast.duration = duration
        return toast
    }
}