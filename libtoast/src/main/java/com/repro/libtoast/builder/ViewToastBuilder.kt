package com.repro.libtoast.builder

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast


class ViewToastBuilder(context: Context) : ToastBuilder(context) {
    private var view: View? = null

    private var layoutId = 0



    fun setView(view: View?): ViewToastBuilder {
        this.view = view
        return this
    }

    fun setLayoutId(layoutId: Int): ViewToastBuilder {
        this.layoutId = layoutId
        return this
    }

    override fun build(): Toast? {
        val viewToast = Toast.makeText(context, "", duration)
        var toastView: View? = null
        toastView = when {
            view != null -> {
                view
            }
            layoutId > 0 -> {
                LayoutInflater.from(context).inflate(layoutId, null)
            }
            else -> {
                throw IllegalArgumentException("view is null or layoutId is illegal")
            }
        }
        viewToast.setView(toastView)
        viewToast.setGravity(gravity, xOffset, yOffset)
        viewToast.duration = duration
        return viewToast
    }
}