package com.repro.libtoast.builder

import android.content.Context
import android.widget.Toast

abstract class ToastBuilder(val context: Context?) {
    protected var duration = 0
    protected var gravity = 0
    protected var xOffset = 0
    protected var yOffset = 0

    fun setDuration(duration: Int): ToastBuilder = apply {
        this.duration = duration
    }

    fun setGravity(gravity: Int): ToastBuilder = apply {
        this.gravity = gravity
        return this
    }

    fun setXOffset(xOffset: Int): ToastBuilder = apply {
        this.xOffset = xOffset
        return this
    }

    fun setYOffset(yOffset: Int): ToastBuilder = apply {
        this.yOffset = yOffset
    }

    abstract fun build(): Toast?
}