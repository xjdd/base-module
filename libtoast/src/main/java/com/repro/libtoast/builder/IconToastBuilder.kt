package com.repro.libtoast.builder

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Drawable
import android.view.Gravity
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast


class IconToastBuilder(context: Context) : ToastBuilder(context){
    private var msg: CharSequence? = null
    private var icon: Drawable? = null

    fun setMsgStr(msg: CharSequence?): IconToastBuilder = apply {
        this.msg = msg
    }

    fun setIcon(icon: Drawable?): IconToastBuilder = apply {
        this.icon = icon
    }

    @SuppressLint("RtlHardcoded")
    override fun build(): Toast? {
        val iconToast = Toast.makeText(context, "", Toast.LENGTH_SHORT)
        iconToast.setGravity(Gravity.CENTER, xOffset, yOffset)
        val toastView = iconToast.view as LinearLayout?
        val tv = toastView!!.getChildAt(0) as TextView
        tv.text = msg
        when {
            gravity and Gravity.LEFT != 0 -> {
                tv.setCompoundDrawablesWithIntrinsicBounds(icon, null, null, null)
            }
            gravity and Gravity.TOP != 0 -> {
                tv.setCompoundDrawablesWithIntrinsicBounds(null, icon, null, null)
            }
            gravity and Gravity.RIGHT != 0 -> {
                tv.setCompoundDrawablesWithIntrinsicBounds(null, null, icon, null)
            }
            gravity and Gravity.BOTTOM != 0 -> {
                tv.setCompoundDrawablesWithIntrinsicBounds(null, null, null, icon)
            }
        }
        tv.gravity = Gravity.CENTER
        iconToast.duration = duration
        return iconToast
    }
}