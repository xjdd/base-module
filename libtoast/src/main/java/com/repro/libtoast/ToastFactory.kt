package com.repro.libtoast

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.View

import android.widget.Toast


/**
 * 创建工厂
 */
class ToastFactory {
    companion object{
        private var toastCreator: ToastCreator? = null

        private fun ToastFactory() {}

        fun setToastCreator(creator: ToastCreator?) {
            toastCreator = creator
        }

        fun getToastCreator(): ToastCreator? {
            if (toastCreator == null) {
                toastCreator = SimpleToastImpl()
            }
            return toastCreator
        }
    }

    /**
     * toast生成器，根据不同需求生成不同toast
     */
    interface ToastCreator {
        /**
         * 弹出普通消息
         *
         * @param context
         * @param msgStr   消息文本
         * @param duration 时长
         * @param gravity  位置
         * @param xOffset
         * @param yOffset
         * @return
         */
        fun show(
            context: Context?,
            msgStr: CharSequence?,
            duration: Int,
            gravity: Int,
            xOffset: Int,
            yOffset: Int
        ): Toast?

        /**
         * 弹出自定义view
         *
         * @param context
         * @param view     需要展示的view
         * @param duration 时长
         * @param gravity  位置
         * @param xOffset
         * @param yOffset
         * @return
         */
        fun showView(
            context: Context?,
            view: View?,
            duration: Int,
            gravity: Int,
            xOffset: Int,
            yOffset: Int
        ): Toast?

        /**
         * 弹出自定义view
         *
         * @param context
         * @param layoutId
         * @param duration
         * @param gravity
         * @param xOffset
         * @param yOffset
         * @return
         */
        fun showLayout(
            context: Context?,
            layoutId: Int,
            duration: Int,
            gravity: Int,
            xOffset: Int,
            yOffset: Int
        ): Toast?

        /**
         * 弹出带图标的文本消息
         *
         * @param context
         * @param msg     消息文本
         * @param icon    提示的图标
         * @param gravity 图标的位置（left, top, right, bottom）
         * @return
         */
        fun showIcon(
            context: Context?,
            msg: CharSequence?,
            icon: Drawable?,
            gravity: Int,
            duration: Int
        ): Toast?
    }
}