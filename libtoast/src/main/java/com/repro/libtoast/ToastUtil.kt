package com.repro.libtoast

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.Gravity
import android.view.View
import android.widget.Toast
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat

/**
 * 统一管理类
 */
object ToastUtil {
    private var DEBUG = false

    fun setDebug(debug: Boolean) {
        ToastUtil.DEBUG = debug
    }

    //==================================================================================

    //==================================================================================
    /**
     * 只在debug模式下显示toast
     */
    fun showDebug(context: Context?, @StringRes msgStr: Int): Toast? {
        return if (!ToastUtil.DEBUG) {
            null
        } else show(context, msgStr, Toast.LENGTH_SHORT)
    }

    /**
     * 只在debug模式下显示toast
     */
    fun showDebug(context: Context?, msg: CharSequence?): Toast? {
        return if (!ToastUtil.DEBUG) {
            null
        } else show(context, msg, Toast.LENGTH_SHORT)
    }

    //==================================================================================

    //==================================================================================
    /**
     * 长时间显示err Toast
     */
    fun showError(context: Context?, @StringRes msgStr: Int): Toast? {
        return showLong(context, msgStr)
    }

    /**
     * 长时间显示err Toast
     */
    fun showError(context: Context?, msgStr: CharSequence?): Toast? {
        return showLong(context, msgStr)
    }

    //==================================================================================

    //==================================================================================
    /**
     * 默认显示的Toast
     */
    fun show(context: Context?, msgStr: CharSequence?): Toast? {
        return show(context, msgStr, Toast.LENGTH_SHORT)
    }

    /**
     * 默认显示的Toast
     */
    fun show(context: Context?, @StringRes msgRes: Int): Toast? {
        return show(context, msgRes, Toast.LENGTH_SHORT)
    }

    /**
     * 长时间显示Toast
     */
    fun showLong(context: Context?, message: CharSequence?): Toast? {
        return show(context, message, Toast.LENGTH_LONG)
    }

    /**
     * 长时间显示Toast
     */
    fun showLong(context: Context?, @StringRes msgRes: Int): Toast? {
        return show(context, msgRes, Toast.LENGTH_LONG)
    }

    //==================================================================================

    //==================================================================================
    /**
     * 自定义显示Toast text
     */
    fun show(context: Context?, @StringRes msgRes: Int, duration: Int): Toast? {
        val msg: CharSequence = context?.getString(msgRes) ?: ""
        return show(context, msg, duration)
    }

    fun show(context: Context?, msgStr: CharSequence?, duration: Int): Toast? {
        return show(context, msgStr, duration, Gravity.CENTER, 0, 0)
    }

    fun show(
        context: Context?,
        msgStr: CharSequence?,
        duration: Int,
        gravity: Int,
        xOffset: Int,
        yOffset: Int
    ): Toast? {
        return ToastFactory.getToastCreator()
            ?.show(context, msgStr, duration, gravity, xOffset, yOffset)
    }

    //==================================================================================

    //==================================================================================
    /**
     * 自定义显示Toast view
     */
    fun showView(context: Context?, view: View?): Toast? {
        return showView(context, view, Toast.LENGTH_LONG, Gravity.CENTER, 0, 0)
    }

    /**
     * 自定义显示Toast view
     */
    fun showView(
        context: Context?,
        view: View?,
        duration: Int,
        gravity: Int,
        xOffset: Int,
        yOffset: Int
    ): Toast? {
        return ToastFactory.getToastCreator()
            ?.showView(context, view, duration, gravity, xOffset, yOffset)
    }

    //==================================================================================

    //==================================================================================
    /**
     * 自定义显示Toast view
     */
    fun showLayout(context: Context?, layoutId: Int): Toast? {
        return showLayout(context, layoutId, Toast.LENGTH_LONG, Gravity.CENTER, 0, 0)
    }

    /**
     * 自定义显示Toast view
     */
    fun showLayout(
        context: Context?,
        layoutId: Int,
        duration: Int,
        gravity: Int,
        xOffset: Int,
        yOffset: Int
    ): Toast? {
        return ToastFactory.getToastCreator()
            ?.showLayout(context, layoutId, duration, gravity, xOffset, yOffset)
    }

    //==================================================================================

    //==================================================================================
    /**
     * 自定义显示Toast icon
     */
    fun showIcon(context: Context?, @DrawableRes iconRes: Int): Toast? {
        return showIcon(context, null, iconRes)
    }

    fun showIcon(context: Context?, icon: Drawable?): Toast? {
        return showIcon(context, null, icon)
    }

    /**
     * 自定义显示Toast icon
     */
    fun showIcon(context: Context?, msg: CharSequence?, @DrawableRes iconRes: Int): Toast? {
        return showIcon(context, msg, context?.let { ContextCompat.getDrawable(it, iconRes) })
    }

    /**
     * 自定义显示Toast icon
     */
    fun showIcon(context: Context?, msg: CharSequence?, icon: Drawable?): Toast? {
        return showIcon(context, msg, icon, Gravity.CENTER, Toast.LENGTH_LONG)
    }

    fun showIcon(
        context: Context?,
        msg: CharSequence?,
        icon: Drawable?,
        gravity: Int,
        duration: Int
    ): Toast? {
        return ToastFactory.getToastCreator()?.showIcon(context, msg, icon, gravity, duration)
    }
}