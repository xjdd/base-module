package com.repro.libtoast

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.View
import android.widget.Toast
import com.repro.libtoast.builder.IconToastBuilder
import com.repro.libtoast.builder.TextToastBuilder


class SimpleToastImpl : ToastFactory.ToastCreator {

    @Override
    override fun show(
        context: Context?,
        msgStr: CharSequence?,
        duration: Int,
        gravity: Int,
        xOffset: Int,
        yOffset: Int
    ): Toast? {
        val toast: Toast? = context?.let {
            TextToastBuilder(it.applicationContext)
                .setMsgStr(msgStr)
                .setDuration(duration)
                .setGravity(gravity)
                .setXOffset(xOffset)
                .setYOffset(yOffset)
                .build()
        }
        toast?.show()
        return toast
    }

    @Override
    override fun showView(
        context: Context?,
        view: View?,
        duration: Int,
        gravity: Int,
        xOffset: Int,
        yOffset: Int
    ): Toast? {
        val toast: Toast? = context?.let {
            ViewToastBuilder(it.applicationContext)
                .setView(view)
                .setDuration(duration)
                .setGravity(gravity)
                .setXOffset(xOffset)
                .setXOffset(yOffset)
                .build()
        }
        toast?.show()
        return toast
    }

    @Override
    override fun showLayout(
        context: Context?,
        layoutId: Int,
        duration: Int,
        gravity: Int,
        xOffset: Int,
        yOffset: Int
    ): Toast? {
        val toast: Toast? = context?.let {
            ViewToastBuilder(it.applicationContext)
                .setLayoutId(layoutId)
                .setDuration(duration)
                .setGravity(gravity)
                .setXOffset(xOffset)
                .setYOffset(yOffset)
                .build()
        }
        toast?.show()
        return toast
    }

    @Override
    override fun showIcon(
        context: Context?,
        msg: CharSequence?,
        icon: Drawable?,
        gravity: Int,
        duration: Int
    ): Toast? {
        val toast: Toast? = context?.let {
            IconToastBuilder(it.applicationContext)
                .setMsgStr(msg)
                .setIcon(icon)
                .setDuration(duration)
                .setGravity(gravity)
                .build()
        }
        toast?.show()
        return toast
    }
}