#### liveDataBus

### 1. LiveDataBus 的封装

1. 通过 map 维护一个消息事件和 MutableLiveData 的映射关系，MutableLiveData 的类型默认为 Object，接收任意类型，实现总线通信
2. 将 LiveDataBus 封装为一个单例类。
3. 消息注册时，如果当前 map 中不存在，则先将消息和对应的 MutableLiveData 对象放入维护的 map 中，添加映射关系，返回当前 map 中缓存的 MutableLiveData 对象

#### 1.1 LiveDataBus 的组成

- **消息** 消息可以是任何的Object，可以定义不同类型的消息，如 Boolean、String。也可以定义自定义类型的消息。
- **消息总线** 消息总线通过单例实现，不同的消息通道存放在一个 HashMap中。
- 订阅 订阅者通过 getChannel获取消息通道，然后调用 observe 订阅这个通道的消息。
- 发布 发布者通过 getChannel获取消息通道，然后调用 setValue 或者 postValue 发布消息。
- 使用setValue自动识别是否在主线程否则自动执行postValue

#### 1.2 使用方法

注册事件，发送消息

```kotlin
  //通过getChannel注册和获取到响应的渠道
  LiveDataBusEvent.get().getChannel("nihao2", String::class.java)
            .observe(this@ActivityDispatchView, { t ->
                DLog.e("----$t+====>ActivityDispatchView")
                ToastUtil.show(this@ActivityDispatchView, t)
          
   })
 //往渠道里面塞数据
 LiveDataBusEvent.get().getChannel("nihao2",String::class.java).setValue("value")
```

#### 1.3liveData存在的问题

一个是粘性数据，一个是数据倒灌，

粘性数据只是我在observer注册之前已经开始发送通道数据，发送数据之后程序才注册observer会接受到之前发送的数据，注册的时候会执行以下`considerNotify()`

```kotlin 
 @SuppressWarnings("WeakerAccess") /* synthetic access */
    void dispatchingValue(@Nullable ObserverWrapper initiator) {
        if (mDispatchingValue) {
            mDispatchInvalidated = true;
            return;
        }
        mDispatchingValue = true;
        do {
            mDispatchInvalidated = false;
            if (initiator != null) {
                considerNotify(initiator);
                initiator = null;
            } else {
   //重点是这里,
              for (Iterator<Map.Entry<Observer<? super T>, ObserverWrapper>> iterator =
                        mObservers.iteratorWithAdditions(); iterator.hasNext(); ) {
                    considerNotify(iterator.next().getValue());
                    if (mDispatchInvalidated) {
                        break;
                    }
                }
            }
        } while (mDispatchInvalidated);
        mDispatchingValue = false;
    }
```

```kotlin
private void considerNotify(ObserverWrapper observer) {
        if (!observer.mActive) {
            return;
        }
        // Check latest state b4 dispatch. Maybe it changed state but we didn't get the event yet.
        //
        // we still first check observer.active to keep it as the entrance for events. So even if
        // the observer moved to an active state, if we've not received that event, we better not
        // notify for a more predictable notification order.
        if (!observer.shouldBeActive()) {
            observer.activeStateChanged(false);
            return;
        }
        if (observer.mLastVersion >= mVersion) {
            return;
        }
        //这里mVersion来一个value事件则就会增加+1,然后赋值给mLastVersion,
//        mLastVersion初始值-1,所以一般情况lastVersion会小于mVersion就会触发
//        观察者的onChanged方法去执行。
        observer.mLastVersion = mVersion;
        observer.mObserver.onChanged((T) mData);
    }
```

**核心原因：** 对于 LiveData，其初始的 version 是-1，当我们调用了其 setValue或者 postValue，其 vesion 会+1；对于每一个观察者的封装 ObserverWrapper，其初始 version 也为-1，也就是说，每一个新注册的观察者，其 version 为-1；当LiveData 设置这个 ObserverWrapper 的时候，如果 LiveData 的 version 大于 ObserverWrapper 的 version，LiveData 就会强制把当前 value 推送给 Observer。

所以当setValue先执行的时候会影响到observer的注册事件，从而导致注册立即会触发已经setValue的事件，这样也就会出现所说的粘性事件，但是因为粘性事件的缘故从而导致当observerWarpper检测到生命周期变化的时候就会出现在次取值执行之前的数据，也就会发生数据倒灌，这个产生会导致界面在不受控制的情况下可能多次触发observer而这不是我们想要的结果。

#### 1.4解决方案

1.不要在observer之前使用setValue。

2.可以通过hook反射方式在observer的时候 去修改mlastVersion是其与mVersion相当。

3.使用一个包装类包裹一下当前observer，重写onchanged事件，并且自己维护一个setValue的序列号 ，使其在只能正向执行，所以observer只能在setValue才能去处理自己的事件否则丢弃事件。