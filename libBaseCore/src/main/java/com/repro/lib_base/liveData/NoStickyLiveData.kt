package com.repro.lib_base.liveData

import android.os.Looper


/**
 * 可以有效防止数据倒灌情况
 */
class NoStickyLiveData<T> : ProtectedSingleLiveData<T>() {

    /**
     * 自动判别是否在主线程，如果在主线程则直接使用setValue,否则走postValue
     */
    @Override
    public override fun setValue(value: T) {
        if (isMainThread()) {
            super.setValue(value)
        } else {
            super.postValue(value)
        }
    }

    @Override
    public override fun postValue(value: T) {
        super.postValue(value)
    }

    class Builder<T> {
        /**
         * 是否允许传入 null value
         */
        private var isAllowNullValue = false
        fun setAllowNullValue(allowNullValue: Boolean): Builder<T> {
            isAllowNullValue = allowNullValue
            return this
        }

        fun create(): NoStickyLiveData<T> {
            val liveData: NoStickyLiveData<T> = NoStickyLiveData()
            liveData.isAllowNullValue = isAllowNullValue
            return liveData
        }
    }

    /**
     * 判断是否是主线程 获取当前looper是否与getMainLooper相等
     */
    fun isMainThread(): Boolean {
        return Looper.myLooper() == Looper.getMainLooper()
    }
}