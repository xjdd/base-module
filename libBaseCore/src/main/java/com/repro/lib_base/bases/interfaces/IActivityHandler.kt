package com.repro.lib_base.bases.interfaces

import android.app.Activity
import android.view.LayoutInflater
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModel

/**
 * 桥接
 */
interface IActivityHandler : LifecycleOwner {
    /**
     * 获取viewModel
     * @param mClass
     * @return T
     */
    fun < T : ViewModel> getViewModel(modelClass: Class<T>): T?

    /**
     * 获取layoutInflate
     */
    fun getLayInflate(): LayoutInflater

    /**
     * 当前界面是否被销毁
     */
    fun isDestroyed(): Boolean


    /**
     * 获取Activity
     */
    fun getActivityContext(): Activity
}