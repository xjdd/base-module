package com.repro.lib_base.bases.activity

import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding

/**
 * DataBinding
 */
abstract class BaseDataBindActivity<DB : ViewDataBinding> : BaseStateActivity() {
    open lateinit var binding: DB

    /**
     * 创建mContentView
     */
    override fun createContainerView(toolbarView: ToolBarView?) {

        binding = DataBindingUtil.setContentView(this, requestLayoutId())
        mContentView = binding.root
        binding.lifecycleOwner = this

        val toolBar = toolbarView?.onCreateToolBar(layoutInflater, this)
        //界面添加toolbar
        toolBar?.let {
            rootView.addView(
                toolBar, ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    toolbarView.getToolbarHeightSpan()
                )
            )
            //是否偏移量
            mContentView.layoutParams = toolbarView.contentTopMarginLayoutParams(isEnableFitSystemWindows)
        }
    }

    /**
     * 设置contentview的layout
     * 必须要实现项
     * @return layoutid
     */
    protected abstract fun requestLayoutId(): Int
}