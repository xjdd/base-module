package com.repro.lib_base.bases

import android.app.Application
import com.repo.logger.LogManagers
import com.repro.com.repro.lib_base.BuildConfig
import com.repro.lib.imageload.ImageManager
import com.repro.lib.imageload.impl.CoilEngine
import com.repro.lib_base.utils.ActivityManagerUtil
import com.repro.lib_base.utils.AppCache
import com.repro.libtoast.ToastUtil

open class BaseApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        //缓存一个全局的application
        AppCache.setContext(this)
        //监控
        LogManagers.getInstance().init(this, BuildConfig.DEBUG)
        //Activity管理栈
        ActivityManagerUtil.get().init(this)
        //Toast设置是否debug弹窗
        ToastUtil.setDebug(BuildConfig.DEBUG)
        //图片加载框架
        ImageManager.initImageLoadOption(this,imageEngineType = CoilEngine())
    }
}