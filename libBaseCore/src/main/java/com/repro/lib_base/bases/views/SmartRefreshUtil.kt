package com.repro.lib_base.bases.views

import androidx.recyclerview.widget.RecyclerView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.loadmore.SimpleLoadMoreView
import com.repro.lib_base.utils.NetUtil
import com.repro.libstate.*
import com.repro.libstate.state.EmptyState
import com.repro.libstate.state.ErrorState
import com.repro.libstate.state.LoadingState
import com.repro.libstate.state.SuccessState
import com.scwang.smart.refresh.layout.SmartRefreshLayout

class SmartRefreshUtil<T>(
    val adapter: BaseQuickAdapter<T, *>,
    private val recyclerView: RecyclerView,
    private val refreshLayout: SmartRefreshLayout,
    private val container: MultiStateContainer?,
    private val preLoadPage: Int,
    private val isNeedLoadMore: Boolean = true,
    private val isNeedRefresh: Boolean = true,
    private val loadData: (page: Int) -> Unit
) {


    private var isLoadMoreing = false
    private var isRefreshing = false
    private var currentPage = 0

    /**
     * 每一页的数据个数
     */
    private var eachPageDataSize = 0

    init {
        refreshLayout.setEnableLoadMore(false)
        adapter.loadMoreModule.isEnableLoadMore = isNeedLoadMore//是否需要加载更多
        if (isNeedLoadMore) {
            adapter.loadMoreModule.preLoadNumber = preLoadPage //当前加载的数量
            val loadMoreView = SimpleLoadMoreView()
            adapter.loadMoreModule.loadMoreView = loadMoreView//添加加载更多的view
//            loadMoreView.setLoadMoreEndGone(true)//加载完成之后之后隐藏
            adapter.loadMoreModule.setOnLoadMoreListener {//监听到展示最后一个item之后加载数据
                loadMore()//加载数据
            }
        }

        refreshLayout.setEnableRefresh(isNeedRefresh)
        if (isNeedRefresh) {
            refreshLayout.setOnRefreshListener {
                refresh()
            }
        }
    }


    /**
     * 获取到分页数据 设置下拉刷新和上拉的状态
     */
    fun onFetchDataFinish(data: MutableList<T>?) {
        onFetchDataFinish(data, true, false)
    }


    /**
     * isHaveHeaderFooter 当recyclerview存在header或者footer时不走container.showEmpty()
     */
    fun onFetchDataFinish(
        data: List<T>?,
        goneIfNoData: Boolean,
        sureLoadMoreEnd: Boolean,
        isHaveHeaderFooter: Boolean = false
    ) {
        refreshLayout.finishRefresh(true)
        if (data != null) {
            if (currentPage == 0 && isRefreshing) {
                eachPageDataSize = data.size
            }

            if (isLoadMoreing) {
                currentPage++
                adapter.addData(data)
            } else {
                adapter.setNewInstance(data as MutableList<T>)
            }

            if (sureLoadMoreEnd) {
                adapter.loadMoreModule.loadMoreEnd(goneIfNoData)
            } else {
                if (data.size < eachPageDataSize) {
                    adapter.loadMoreModule.loadMoreEnd(goneIfNoData)
                } else {
                    adapter.loadMoreModule.loadMoreComplete()
                }
            }

            if (currentPage == 0) {
                if (data.isEmpty()) {
                    if (!isHaveHeaderFooter) {
                        container?.showEmpty<EmptyState>()
                    } else {
                        container?.showSuccess<SuccessState>()
                    }
                } else {
                    container?.showSuccess<SuccessState>()
                }
            } else {
                if (adapter.data.isEmpty()) {
                    if (!isHaveHeaderFooter) {
                        container?.showEmpty<EmptyState>()
                    } else {
                        container?.showSuccess<SuccessState>()
                    }
                } else {
                    container?.showSuccess<SuccessState>()
                }
            }
        }else{
            container?.showEmpty<EmptyState>()
        }
        isLoadMoreing = false
        isRefreshing = false
    }

    fun onFetchDataFinish(
        data: List<T>?,
        goneIfNoData: Boolean,
        isHaveHeaderFooter: Boolean = false
    ) {
        onFetchDataFinish(data, goneIfNoData, false, isHaveHeaderFooter)

    }

    /**
     * 加载错误
     */
    fun loadError() {
        if (isRefreshing) {
            refreshLayout.finishRefresh(false)
        } else {
            adapter.loadMoreModule.loadMoreFail()
        }

        val disconnected = !NetUtil.isNetworkAvailable(recyclerView.context)
        if (disconnected) {
            container?.showError<ErrorState>()
        } else {
            container?.showError<ErrorState>()
        }
        isLoadMoreing = false
        isRefreshing = false
    }

    /**
     * isShowLoading
     * 用于判断是否是下拉刷新时不展示中间的loading
     */
    fun refresh(isShowLoading: Boolean = false) {
        if (isRefreshing || isLoadMoreing) {
            if (isRefreshing) {
                isRefreshing = false
            }
            refreshLayout.finishRefresh(true)
            return
        }
        isRefreshing = true
        if (isShowLoading) {
            container?.showLoading<LoadingState>()
        }
        currentPage = 0
        loadData(0)
    }

    private fun loadMore() {
        if (isRefreshing || isLoadMoreing) {
            if (isLoadMoreing) {
                isLoadMoreing = false
            }
            return
        }
        isLoadMoreing = true
        loadData(currentPage + 1)
    }
}