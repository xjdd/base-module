package com.repro.lib_base.bases.fragment

import android.view.LayoutInflater
import android.view.View
import androidx.viewbinding.ViewBinding
import java.lang.reflect.ParameterizedType


/**
 * BaseFragment viewBind基类
 */
abstract class BaseFragment<VB : ViewBinding> : FinalStateFragment() {
    protected var binding: VB? = null
    override fun onFragmentCreateStart(): View? {
        super.onFragmentCreateStart()
        binding = getViewBindingForFragment(layoutInflater)
        rooView = binding?.root
        return rooView
    }

    @Suppress("UNCHECKED_CAST")
    private fun getViewBindingForFragment(layoutInflater: LayoutInflater): VB {
        val type = javaClass.genericSuperclass as ParameterizedType
        val aClass = type.actualTypeArguments[0] as Class<*>
        val method = aClass.getDeclaredMethod("inflate", LayoutInflater::class.java)
        return method.invoke(null, layoutInflater) as VB
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}