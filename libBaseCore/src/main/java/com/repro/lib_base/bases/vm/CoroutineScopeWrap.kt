package com.repro.lib_base.bases.vm

import kotlinx.coroutines.CoroutineScope

//创建callback
class CoroutineScopeWrap {
    //挂起函数代码块 返回执行挂起函数
    var doWork: (suspend CoroutineScope.() -> Unit) = {}

    //错误状态 并给出抛出异常
    var doError: (e: Exception) -> Unit = {}

    //完成方法,无参数通知
    var doCompleter: () -> Unit = {}

    fun doWork(call: suspend CoroutineScope.() -> Unit) {
        this.doWork = call
    }

    fun doError(error: (e: java.lang.Exception) -> Unit) {
        this.doError = error
    }

    fun doCompleter(call: () -> Unit) {
        this.doCompleter = call
    }
}