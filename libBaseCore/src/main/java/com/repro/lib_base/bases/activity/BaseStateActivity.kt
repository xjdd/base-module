package com.repro.lib_base.bases.activity

import android.view.View
import com.repro.lib_base.utils.ClickUtil
import com.repro.libstate.MultiStateContainer
import com.repro.libstate.bindMultiState

/**
 * stateView 异常页面创建
 */
abstract class BaseStateActivity : FinalActivity() {
    lateinit var multiStateContainer: MultiStateContainer

    override fun initView(mContentView: View?) {
        multiStateContainer = this.bindMultiState{
            if (ClickUtil.isClickAvailable()) {
                reLoadData()
            }
        }
    }

    /**
     * 重置接口数据
     */
    open fun reLoadData() {}

}