package com.repro.lib_base.bases.fragment

import android.view.View
import com.repro.lib_base.utils.ClickUtil
import com.repro.libstate.MultiStateContainer
import com.repro.libstate.bindMultiState

/**
 * 响应状态state页面，通过multiStateContainer可以设置需要呈现的状态页面
 * @see EmptyState 空状态
 * @see ErrorState 错误状态
 * @see LoadingState 加载页面
 * @see SuccessState 成功页面
 */
abstract class FinalStateFragment : FinalFragment() {

    lateinit var multiStateContainer: MultiStateContainer

    override fun initState(view: View?): View? {
        multiStateContainer = view?.bindMultiState {
            if (ClickUtil.isClickAvailable()) {
                reLoadData()
            }
        }!!
        return multiStateContainer
    }

    /**
     * 点击响应刷新
     */
    protected abstract fun reLoadData()
}