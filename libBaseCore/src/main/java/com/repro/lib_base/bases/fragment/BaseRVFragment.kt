package com.repro.lib_base.bases.fragment

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.chad.library.adapter.base.BaseQuickAdapter
import com.repro.lib_base.bases.views.SmartRecyclerView
import com.scwang.smart.refresh.header.ClassicsHeader
import com.scwang.smart.refresh.layout.api.RefreshHeader

/**
 *description:带recyclerview的fragment
 */
abstract class BaseRVFragment<VB : ViewBinding, T> : BaseFragment<VB>() {
    private val preLoadPage = 1
    abstract val smartRecyclerView: SmartRecyclerView<T>
    abstract val adapter: BaseQuickAdapter<T, *>
    abstract val layoutManager: RecyclerView.LayoutManager

    abstract val loadData: ((page: Int) -> Unit)

    override fun initView(rootView: View?) {
        smartRecyclerView.recyclerView?.layoutManager = layoutManager
        smartRecyclerView.setRefreshHeader(getRefreshHeader())
        smartRecyclerView.setSmartRefreshUtil(
            adapter, preLoadPage, loadMoreNeed(), refreshNeed(), loadData
        )
        if (isRefreshAtOnStart()) {
            smartRecyclerView.startRefresh()
        }
    }

    //头部刷子
    open fun getRefreshHeader(): RefreshHeader {
        return ClassicsHeader(context)
    }

    //是否需要加载更多
    open fun loadMoreNeed(): Boolean {
        return true
    }

    //是否需要刷新
    open fun refreshNeed(): Boolean {
        return true
    }

    //重新刷新
    open fun isRefreshAtOnStart(): Boolean {
        return true
    }
}