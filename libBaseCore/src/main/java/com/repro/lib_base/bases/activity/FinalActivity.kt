package com.repro.lib_base.bases.activity

import android.app.Activity
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.gyf.immersionbar.ImmersionBar
import com.repro.lib_base.bases.interfaces.IActivityHandler
import com.repro.lib_base.utils.SoftInputUtil

/**
 * FinalActivity最终实现类，实现了toolbar,状态栏的
 */
abstract class FinalActivity : AppCompatActivity(), IActivityHandler {
    protected lateinit var rootView: ViewGroup
    protected lateinit var mContentView: View
    protected var time: Long = -1
    protected var isEnableFitSystemWindows = false//是否设置了 fitSystemWindows
    protected var toolbarView: ToolBarView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //创建toolbar
        setThemes(darkFont = true, fitSystemWindow = true)
        rootView = findViewById(android.R.id.content)//提供了视图的根元素
        toolbarView = onToolbarSetting()
        createContainerView(toolbarView)

        //业务开始初始化
        initView(mContentView)
        //observe在这里创建
        observeLiveData()
        //初始化view相关的数据
        setViewData(savedInstanceState)
        //activity创建结束
        onActivityCreateEnd()
    }

    /**
     * 主题样式和toolbar在此设置
     */
    protected abstract fun onToolbarSetting(): ToolBarView?

    /**
     * mContentView 创建方式
     */
    protected open fun createContainerView(toolbarView: ToolBarView?) {

    }

    /**
     * 界面创建成功开始初始化
     *
     *  @param mContentView
     */
    protected abstract fun initView(mContentView: View?)


    protected abstract fun observeLiveData()


    /**
     * 初始化view相关联的数据
     *
     * @param savedInstanceState
     */
    protected abstract fun setViewData(savedInstanceState: Bundle?)

    /**
     * 结束activity创建
     */
    protected open fun onActivityCreateEnd() {}

    /**
     * 状态栏设置
     * @param darkFont 状态栏文字颜色 true:深色,false高亮
     * @param fitSystemWindow 是否填充系统状态栏，true表示默认填充，需要给状态设置颜色否则为透明，false则不填充直接到顶
     */
    open fun setThemes(darkFont: Boolean, fitSystemWindow: Boolean) {
        isEnableFitSystemWindows = fitSystemWindow
        ImmersionBar.with(this)
            .statusBarDarkFont(darkFont)   //状态栏字体是深色，不写默认为亮色
            .fullScreen(true)      //有导航栏的情况下，activity全屏显示，也就是activity最下面被导航栏覆盖，不写默认非全屏
            .fitsSystemWindows(isEnableFitSystemWindows)    //解决状态栏和布局重叠问题，任选其一，默认为false，当为true时一定要指定statusBarColor()，不然状态栏为透明色
            .keyboardEnable(false)  //解决软键盘与底部输入框冲突问题，默认为false，还有一个重载方法，可以指定软键盘mode
            .init();  //必须调用方可沉浸式
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        toolbarView?.onCreateMenuView(this, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
        //销毁界面释放资源
        SoftInputUtil.hideSoftInputView(this)
    }


    override fun <T : ViewModel> getViewModel(modelClass: Class<T>): T? {
        return ViewModelProvider(this).get(modelClass)
    }

    override fun getLayInflate(): LayoutInflater {
        return LayoutInflater.from(this)
    }

    override fun getActivityContext(): Activity {
        return this
    }

    override fun isDestroyed(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return super.isDestroyed();
        } else {
            if (supportFragmentManager.isDestroyed) {
                return true;
            }
        }
        return false;
    }

}


