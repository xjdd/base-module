package com.repro.lib_base.bases.fragment

import android.view.LayoutInflater
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding


/**
 * DataBinding
 */
abstract class BaseDBFragment<DB : ViewDataBinding> : FinalStateFragment() {
    open lateinit var binding: DB
    override fun onFragmentCreateStart(): View? {
        super.onFragmentCreateStart()
        val view = createContainerView(getLayInflate(), requestLayoutId())
        if (view != null) {
            val viewTag = view.tag
            if (viewTag != null && viewTag is String) {
                binding = DataBindingUtil.bind<ViewDataBinding>(view) as DB
                rooView = binding.root
                binding.lifecycleOwner = this
            }
        }
        return rooView
    }

    /**
     * 创建containerview
     *
     * @param inflater
     * @param layoutResID 你自己的布局id
     * @return
     */
    protected open fun createContainerView(inflater: LayoutInflater, layoutResID: Int): View? {
        return if (layoutResID <= 0) {
            null
        } else inflater.inflate(layoutResID, null)
    }

    /**
     * 设置的layout
     * 必须要实现项
     * @return layoutId
     */
    protected abstract fun requestLayoutId(): Int
}