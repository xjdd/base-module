package com.repro.lib_base.bases.activity

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.chad.library.adapter.base.BaseQuickAdapter
import com.repro.lib_base.bases.views.SmartRecyclerView
import com.scwang.smart.refresh.header.ClassicsHeader
import com.scwang.smart.refresh.layout.api.RefreshHeader

/**
 * 带列表的Activity实现
 * 需要业务实现几个方法：
 *  1.列表使用自定义的smartRecyclerView
 */
abstract class BaseRVActivity<VB : ViewBinding, T> : BaseActivity<VB>() {
    private val preLoadPage = 1
    abstract val smartRecyclerView: SmartRecyclerView<T>
    abstract val adapter: BaseQuickAdapter<T, *>
    abstract val layoutManager: RecyclerView.LayoutManager

    abstract val loadData: ((page: Int) -> Unit)

    override fun initView(mContentView: View?) {
        super.initView(mContentView)
        smartRecyclerView.recyclerView?.layoutManager = layoutManager
        smartRecyclerView.setRefreshHeader(getRefreshHeader())
        smartRecyclerView.setSmartRefreshUtil(
            adapter,preLoadPage,loadMoreNeed(),refreshNeed(),loadData
        )
        if (isRefreshAtOnStart()){
            smartRecyclerView.startRefresh()
        }
    }

    //头部刷子
    open fun getRefreshHeader(): RefreshHeader {
        return ClassicsHeader(this)
    }

    //是否需要加载更多
    open fun loadMoreNeed(): Boolean {
        return true
    }

    //是否需要刷新
    open fun refreshNeed(): Boolean {
        return true
    }

    //重新刷新
    open fun isRefreshAtOnStart():Boolean{
        return true
    }
}