package com.repro.lib_base.bases.fragment

import android.view.View


abstract class BaseNoVBFragment : FinalStateFragment() {
    override fun onFragmentCreateStart(): View? {
        super.onFragmentCreateStart()
        if (requestLayoutId() > 0) {
            rooView = getLayInflate().inflate(requestLayoutId(), null)
        } else {
            throw RuntimeException("createView filed!! is null layout ID")
        }
        return rooView
    }

    abstract fun requestLayoutId(): Int
}