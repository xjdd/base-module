package com.repro.lib_base.bases.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.recyclerview.widget.RecyclerView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.repro.com.repro.lib_base.R
import com.repro.libstate.MultiStateContainer
import com.repro.libstate.OnRetryEventListener
import com.scwang.smart.refresh.layout.SmartRefreshLayout
import com.scwang.smart.refresh.layout.api.RefreshHeader

/***
 * 自定义RecyclerView
 */
class SmartRecyclerView<T> : FrameLayout {
    var recyclerView: RecyclerView? = null
        private set
    var smartRefreshLayout: SmartRefreshLayout? = null
    private var smartRefreshUtil:SmartRefreshUtil<T>? = null
    var multiStateContainer: MultiStateContainer? = null
    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(context)
    }

    private fun init(context: Context) {
        val view = LayoutInflater.from(context).inflate(
            R.layout.layout_refresh_recyclerview,
            this,
            false
        )
        multiStateContainer = view.findViewById(R.id.multistate_container)
        recyclerView = view.findViewById(R.id.recyclerView)
        smartRefreshLayout = view.findViewById(R.id.refreshLayout)
        addView(view)

        multiStateContainer?.onRetryEventListener = OnRetryEventListener {
            startRefresh()
        }
    }

    fun setRefreshHeader(refreshHeader: RefreshHeader) {
        smartRefreshLayout?.setRefreshHeader(refreshHeader)
    }

    /**
     * 刷新
     * @param isShowLoading 是否展示loading
     */
    fun startRefresh(isShowLoading: Boolean = false) {
        smartRefreshUtil?.refresh(isShowLoading)
    }

    /**
     * 告诉view获取失败
     */
    fun onFetchDataError() {
        smartRefreshUtil?.loadError()
    }

    /**
     * 请求成功　smartRefreshHelper处理页数记录空视图的显示
     *
     * @param goneIfNoData  已经到底了一直显示
     */
    fun onFetchDataFinish(
        data: List<T>?,
        goneIfNoData: Boolean,
        isHaveHeaderFooter: Boolean = false
    ) {
        smartRefreshUtil?.onFetchDataFinish(data, goneIfNoData, isHaveHeaderFooter)
    }

    /**
     * 请求成功　smartRefreshHelper处理页数记录空视图的显示
     *
     * @param sureLoadMoreEnd  很明确没有下一页了　不需要请求下一页来确认
     */
    fun onFetchDataFinish(
        data: MutableList<T>?,
        goneIfNoData: Boolean,
        sureLoadMoreEnd: Boolean,
        isHaveHeaderFooter: Boolean = false
    ) {
        smartRefreshUtil?.onFetchDataFinish(data, goneIfNoData, sureLoadMoreEnd, isHaveHeaderFooter)
    }


    fun setSmartRefreshUtil(
        adapter: BaseQuickAdapter<T, *>,
        preLoadPage: Int,
        isNeedLoadMore: Boolean = true,
        isNeedRefresh: Boolean = true,
        loadData: (page: Int) -> Unit
    ) {
        recyclerView?.adapter = adapter
        smartRefreshUtil = SmartRefreshUtil(
            adapter,
            recyclerView!!,
            smartRefreshLayout!!,
            multiStateContainer,
            preLoadPage,
            isNeedLoadMore,
            isNeedRefresh,
            loadData
        )

    }

}