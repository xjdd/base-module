package com.repro.lib_base.bases.activity

import android.view.LayoutInflater
import android.view.ViewGroup
import com.repo.utils.DLog

/**
 * 不使用ViewBinding的模式进行view设置
 */
abstract class BaseActivityNoVB : BaseStateActivity() {

    /**
     * 创建mContentView
     */
    override fun createContainerView(toolbarView: ToolBarView?) {
        //创建页面布局
        if (requestLayoutId() > 0) {
            mContentView = LayoutInflater.from(this).inflate(requestLayoutId(), null)
        }
        //判断页面是否初始化成功
        if (mContentView == null) {
            DLog.e("the activity view inflater error")
            finish()
            return
        }

        // 设置ContentView
        rootView.addView(
            mContentView, ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
        )
        //设置toolbar
        val toolBar = toolbarView?.onCreateToolBar(layoutInflater, this)
        //界面添加toolbar
        toolBar?.let {
            rootView.addView(
                toolBar, ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    toolbarView.getToolbarHeightSpan()
                )
            )
            //是否偏移量
            mContentView.layoutParams = toolbarView.contentTopMarginLayoutParams(isEnableFitSystemWindows)
        }
        return
    }

    /**
     * 设置contentview的layout
     * 必须要实现项
     * @return layoutid
     */
    protected abstract fun requestLayoutId(): Int
}