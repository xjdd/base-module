package com.repro.lib_base.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.view.TouchDelegate;
import android.view.View;
import android.view.WindowManager;

/**
 * @Author Yu
 * @Date 2021/6/1 19:57
 * @Description TODO
 */
public class ScreenUtil {
    public static int dip2px(float dipValue) {
        final float scale = Resources.getSystem().getDisplayMetrics().density;
        return (int) (dipValue * scale + 0.5f);
    }


    public static int getScreenWidth(Context context){
        WindowManager wm = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        return wm.getDefaultDisplay().getWidth();
    }

    /**
     * 获取状态栏高度
     *
     * @return
     */
    public static int getStatusBarHeight(View view) {
        Rect outRect = new Rect();
        view.getWindowVisibleDisplayFrame(outRect);
        return outRect.top;
    }

    /**
     * 判断当前软键盘是否显示
     */
    public static Boolean isSoftShowing(View view) {
        return getScreenTotalHeight(view) - getVisibleHeight(view) >100;
    }


    /**
     * 获取屏幕高度
     */
    public static int getScreenTotalHeight(View view) {
        return view.getResources().getDisplayMetrics().heightPixels;
    }

    /**
     * 获取窗口可见区域高度
     */
    public static int getVisibleHeight(View view) {
        Rect outRect = new Rect();
        view.getWindowVisibleDisplayFrame(outRect);
        return outRect.bottom;
    }

    public static void setTouchDelegate(final View view, final int expandTouchWidth) {
        final View parentView = (View) view.getParent();
        parentView.post(new Runnable() {
            @Override
            public void run() {
                final Rect rect = new Rect();
                view.getHitRect(rect);
                rect.top -= expandTouchWidth;
                rect.bottom += expandTouchWidth;
                rect.left -= expandTouchWidth;
                rect.right += expandTouchWidth;
                TouchDelegate touchDelegate = new TouchDelegate(rect, view);
                parentView.setTouchDelegate(touchDelegate);
            }
        });
    }


}
