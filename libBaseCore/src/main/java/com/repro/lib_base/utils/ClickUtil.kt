package com.repro.lib_base.utils

object ClickUtil {
    // 双击事件记录最近一次点击的时间
    private var lastClickTime: Long = 0

    /**
     * 按钮点击去重实现方法一 ，需要在onclick回调内调用
     *
     * @param miniIntervalMills 最小间隔。
     */
    fun isClickAvailable(miniIntervalMills : Long = 500): Boolean {
        if (System.currentTimeMillis() - lastClickTime > miniIntervalMills) {
            lastClickTime = System.currentTimeMillis()
            return true
        }
        return false
    }
}