package com.repro.lib_base.utils

/**
 * String过滤字符串
 */
fun String.filter(filter:(Char) -> Boolean):String{
   val sb = StringBuilder()
    for (index in 0 .. sb.length){
        val element = get(index)
        if(filter(element)){
            sb.append(element)
        }
    }
    return sb.toString()
}
