package com.repro.lib_base.utils

import android.net.Uri
import android.os.Build
import androidx.core.content.FileProvider
import java.io.File
import java.lang.Exception

/**
 * @Author Yu
 * @Date 2021/6/3 13:45
 * @Description TODO
 */
object FileUtil {
    var fileProviderPath = AppCache.getContext().packageName + ".fileprovider"

    fun getUrl(imgPath: String): Uri? {
        try {
            var uri = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                FileProvider.getUriForFile(
                    AppCache.getContext(),
                    fileProviderPath, File(imgPath)
                )
            } else {
                Uri.fromFile(File(imgPath))
            }
            return uri
        } catch (s: Exception) {

        }
        return null
    }

    // 获取文件鲁目录大小
    fun getFileSize(dir: File?): Long {
        if (dir == null) {
            return 0
        }
        if (!dir.isDirectory) {
            return 0
        }
        var dirSize = 0L
        var files = dir.listFiles()
        for (value in files) {
            if (value.isFile) {
                dirSize += value.length()
            } else if (value.isDirectory) {
                dirSize += value.length()
                dirSize += getFileSize(value)
            }
        }
        return dirSize
    }

    // 删除指定目录
    fun deleteTargetFile(dir: File?): Boolean {
        if (dir == null) {
            return false
        }
        if (dir.isDirectory) {
            var children = dir.list() //递归删除目录中的子目录下
            for (value in children) {
                /**
                 * 礼物的动画资源不清除
                 */
                if (value == "giftResource"){
                    break
                }
                val success = deleteTargetFile(File(dir, value))
                if (!success) {
                    return false
                }
            }
        }
        // 目录此时为空，可以删除
        return dir.delete()
    }
}