package com.repro.lib_base.utils

import java.math.BigDecimal

/**
 * 计算器
 */
object CalculateUtil {

    /**
     * 三位数处除法
     */
    fun delete(a: Float, b: Float, c: Float): Float {
        val y = BigDecimal(a.toString())
        val x = BigDecimal(b.toString())
        val z = BigDecimal(c.toString())
        val result = y.subtract(x).subtract(z)
        return result.toFloat()
    }

    /**
     * 整数乘法
     */
    fun multiply(a: Int, num: Int): Int {
        val x = BigDecimal(a.toString())
        val y = BigDecimal(num.toString())
        val result = x.multiply(y)
        return result.toInt()
    }


}