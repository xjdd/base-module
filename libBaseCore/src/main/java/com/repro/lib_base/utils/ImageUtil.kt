package com.repro.lib_base.utils

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import java.io.IOException

/**
 * @Author Yu
 * @Date 2021/5/31 19:42
 * @Description TODO
 */
class ImageUtil {
    companion object {
        fun getImageSize(path: String?): IntArray? {
            var size = IntArray(2)
            try {
                var onlyBoundsOptions = BitmapFactory.Options()
                onlyBoundsOptions.inJustDecodeBounds = true
                BitmapFactory.decodeFile(path, onlyBoundsOptions)
                var originalWidth = onlyBoundsOptions.outWidth
                var originalHeight = onlyBoundsOptions.outHeight
                var degree = path?.let { getBitmapDegree(it) }
                if (degree == 0) {
                    size[0] = originalWidth
                    size[1] = originalHeight
                } else {
                    //图片分辨率以480x800为标准
                    var hh = 800f //这里设置高度为800f
                    var ww = 480f //这里设置宽度为480f
                    if (degree == 90 || degree == 270) {
                        hh = 480f
                        ww = 800f
                    }
                    //缩放比。由于是固定比例缩放，只用高或者宽其中一个数据进行计算即可
                    var be = 1 //be=1表示不缩放
                    if (originalWidth > originalHeight && originalWidth > ww) { //如果宽度大的话根据宽度固定大小缩放
                        be = (originalWidth / ww).toInt()
                    } else if (originalWidth < originalHeight && originalHeight > hh) { //如果高度高的话根据宽度固定大小缩放
                        be = (originalHeight / hh).toInt()
                    }
                    if (be <= 0) be = 1
                    val bitmapOptions = BitmapFactory.Options()
                    bitmapOptions.inSampleSize = be //设置缩放比例
                    bitmapOptions.inDither = true //optional
                    bitmapOptions.inPreferredConfig = Bitmap.Config.ARGB_8888 //optional
                    var bitmap = BitmapFactory.decodeFile(path, bitmapOptions)
                    bitmap = degree?.let { rotateBitmapByDegree(bitmap, it) }
                    size[0] = bitmap.width
                    size[1] = bitmap.height
                }
            } catch (e: Exception) {
                e.printStackTrace();
            }

            return size
        }


        // 读取图片旋转角度
        fun getBitmapDegree(fileName: String): Int {
            var degree = 0
            try {  // 从指定路径读取图片 并获取EXIF信息
                var exifInterface = ExifInterface(fileName)
                // 获取图片旋转信息
                var orientation = exifInterface.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL
                )
                when (orientation) {
                    ExifInterface.ORIENTATION_ROTATE_90 -> degree = 90
                    ExifInterface.ORIENTATION_ROTATE_180 -> degree = 180
                    ExifInterface.ORIENTATION_ROTATE_270 -> degree = 270
                }
            } catch (e: IOException) {
                e.printStackTrace();
            }
            return degree
        }

        /**
         * 将图片按照某个角度进行旋转
         *
         * @param bm     需要旋转的图片
         * @param degree 旋转角度
         * @return 旋转后的图片
         */
        fun rotateBitmapByDegree(bm: Bitmap, degree: Int): Bitmap? {
            var returnBm: Bitmap? = null
            // 根据旋转角度，生成旋转矩阵
            val matrix = Matrix()
            matrix.postRotate(degree.toFloat())
            try {
                // 将原始图片按照旋转矩阵进行旋转，并得到新的图片
                returnBm = Bitmap.createBitmap(bm, 0, 0, bm.width, bm.height, matrix, true)
            } catch (e: OutOfMemoryError) {
            }
            if (returnBm == null) {
                returnBm = bm
            }
            if (bm != returnBm) {
                bm.recycle()
            }
            return returnBm
        }



    }
}