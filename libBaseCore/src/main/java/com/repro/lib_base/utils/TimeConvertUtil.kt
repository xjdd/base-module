package com.repro.lib_base.utils

import java.util.*

/**
 *@date 2021/7/6 11:48
 *description:
 */
object TimeConvertUtil {

    /**
     * 秒转时分秒
     */
    fun convertHours(time: Long): String {
        var convertTime = ""
        var hour = 0
        var min = 0
        var sec = 0
        if (time <= 0) {
            return "00:00:00"
        }

        min = (time / 60).toInt()
        if (min < 60) {
            sec = (time % 60).toInt()
            convertTime = "00:" + unitFormat(min) + ":" + unitFormat(sec)
        } else {
            hour = min / 60
            if (hour > 99) {
                return "99:59:59"
            }
            min %= 60
            sec = (time - hour * 3600 - min * 60).toInt()
            convertTime = unitFormat(hour) + ":" + unitFormat(min) + ":" + unitFormat(sec)
        }

        return convertTime
    }

    /**
     * 秒转化成分秒
     */
    fun convertMin(time:Long):String{
        var convertTime = ""
        var min = 0
        var sec = 0
        if (time <= 0) {
            return "00:00"
        }else{
            min =  (time / 60).toInt()
            sec = (time % 60).toInt()
            convertTime = unitFormat(min)+":"+ unitFormat(sec)
        }

        return convertTime
    }


    fun unitFormat(i: Int): String {
        return if (i in 0..9) {
            "0$i"
        } else {
            i.toString()
        }
    }


    /**
     * 获取当前的月日信息，结果样式：09/07
     *
     * @return
     */
    fun getMonthAndDay(): String {
        val instance = Calendar.getInstance()
        val month = instance.get(Calendar.MONTH) + 1
        val day = instance.get(Calendar.DAY_OF_MONTH)
        val monthStr = if (month < 10) {"0$month"} else {month.toString()}
        val dayStr = if (day < 10) {"0$day"} else {day.toString()}
        return "$monthStr/$dayStr"
    }

}