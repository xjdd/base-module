package com.repro.lib_base.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DataUtils {

    /**
     * 时间戳转日期 年
     * @param milSecond
     * @return
     */
    public static String getYearDate(long milSecond) {
        Date date = new Date(milSecond);
        SimpleDateFormat format = new SimpleDateFormat("yyyy");
        return format.format(date);
    }

    /**
     * 时间戳转日期
     * @param milSecond
     * @return
     */
    public static String getDateToString(long milSecond) {
        Date date = new Date(milSecond);
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
        return format.format(date);
    }


    /**
     * 时间戳转日期
     * @param milSecond
     * @return
     */
    public static String getDateToStringDetailed(long milSecond) {
        Date date = new Date(milSecond);
        SimpleDateFormat format = new SimpleDateFormat("MM/dd hh:mm");
        return format.format(date);
    }

    /**
     * 根据返回的时间戳中的时间与当前年份比较的结果返回时间字符串
     * @return
     */
    public static String getDataByYear(long milSecond){
        String yearData = getYearDate(milSecond);
        if(yearData.equals(String.valueOf(getYear()))){//今年的消息
            return getDateToStringDetailed(milSecond);
        }else {//往年的消息
            return getDateToString(milSecond);
        }
    }

    /**
     * 获取年
     * @return
     */
    public static int getYear(){
        Calendar cd = Calendar.getInstance();
        return  cd.get(Calendar.YEAR);
    }
    /**
     * 获取月
     * @return
     */
    public static int getMonth(){
        Calendar cd = Calendar.getInstance();
        return  cd.get(Calendar.MONTH)+1;
    }
    /**
     * 获取日
     * @return
     */
    public static int getDay(){
        Calendar cd = Calendar.getInstance();
        return  cd.get(Calendar.DATE);
    }
    /**
     * 获取时
     * @return
     */
    public static int getHour(){
        Calendar cd = Calendar.getInstance();
        return  cd.get(Calendar.HOUR);
    }
    /**
     * 获取分
     * @return
     */
    public static int getMinute() {
        Calendar cd = Calendar.getInstance();
        return cd.get(Calendar.MINUTE);
    }
}
