#### /   架构介绍   /

一个良好的架构需要什么，根据设计原则，有以下：

- 实现项目所需要的功能，为业务需求打下基础

- 可扩展性、可配置性足够强大

- 易用性，方便新成员学习和上手

- 代码高可复用性，添加新功能的时候可以重用大部分已有代码

下面先来看一下项目libBase的包划分

    |api                         接口配置

    |bases                    BaseActivity BaseFragment,

    |bean                      业务bean

    |comment              公共配置

    |constants             常量

    |network                网络相关配置

    |utils                        工具类

    |widget                   小组件

    